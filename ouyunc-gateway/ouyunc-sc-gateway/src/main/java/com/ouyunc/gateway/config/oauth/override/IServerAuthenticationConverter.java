package com.ouyunc.gateway.config.oauth.override;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.resource.web.server.ServerBearerTokenAuthenticationConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author fangzhenxun
 * @Description:  从请求参数或请求头中提取参数并进行转换成Authentication
 * @Version V1.0
 **/
@Component
public class IServerAuthenticationConverter extends ServerBearerTokenAuthenticationConverter {



    /**
     * @Author fangzhenxun
     * @Description
     * @param exchange
     * @return reactor.core.publisher.Mono<org.springframework.security.core.Authentication>
     */
    @Override
    public Mono<Authentication> convert(ServerWebExchange exchange) {
        super.setAllowUriQueryParameter(true);
        return super.convert(exchange);
    }
}
