package com.ouyunc.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient // 该注解在该版本后可省略
@SpringBootApplication
public class OuyuncScGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(OuyuncScGatewayApplication.class, args);
    }

}
