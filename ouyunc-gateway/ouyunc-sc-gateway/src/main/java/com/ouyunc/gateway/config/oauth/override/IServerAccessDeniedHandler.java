package com.ouyunc.gateway.config.oauth.override;

import com.alibaba.fastjson.JSON;
import com.ouyunc.common.base.ResponseResult;
import com.ouyunc.common.constant.enums.ResponseCodeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.server.authorization.ServerAccessDeniedHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author fangzhenxun
 * @Description: 鉴权（可能没有权限）失败处理器
 * @Version V1.0
 **/
@Component
public class IServerAccessDeniedHandler implements ServerAccessDeniedHandler {
    private static Logger log = LoggerFactory.getLogger(IServerAccessDeniedHandler.class);

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, AccessDeniedException e) {
        ServerHttpResponse response = exchange.getResponse();
        // 设置编码
        response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        // 设置状态
        response.setStatusCode(HttpStatus.FORBIDDEN);
        // 将信息转换为 JSON   ResponseResult.error(ResponseCodeEnum.FORBIDDEN)
        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(JSON.toJSONBytes(ResponseResult.error(ResponseCodeEnum.FORBIDDEN)));
        return response.writeWith(Mono.just(buffer));
    }
}
