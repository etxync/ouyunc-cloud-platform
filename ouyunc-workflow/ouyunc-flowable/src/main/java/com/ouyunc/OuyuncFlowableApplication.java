package com.ouyunc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OuyuncFlowableApplication {

    public static void main(String[] args) {
        SpringApplication.run(OuyuncFlowableApplication.class, args);
    }

}
