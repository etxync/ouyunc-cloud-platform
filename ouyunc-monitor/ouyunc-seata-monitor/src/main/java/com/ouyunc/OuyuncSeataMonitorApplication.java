package com.ouyunc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OuyuncSeataMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(OuyuncSeataMonitorApplication.class, args);
    }

}
