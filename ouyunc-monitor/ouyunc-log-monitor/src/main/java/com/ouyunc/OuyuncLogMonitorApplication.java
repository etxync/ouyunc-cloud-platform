package com.ouyunc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OuyuncLogMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(OuyuncLogMonitorApplication.class, args);
    }

}
