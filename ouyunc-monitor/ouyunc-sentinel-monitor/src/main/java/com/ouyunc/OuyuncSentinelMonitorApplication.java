package com.ouyunc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OuyuncSentinelMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(OuyuncSentinelMonitorApplication.class, args);
    }

}
