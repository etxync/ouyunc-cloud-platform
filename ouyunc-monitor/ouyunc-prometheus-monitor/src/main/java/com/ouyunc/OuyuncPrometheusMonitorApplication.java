package com.ouyunc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OuyuncPrometheusMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(OuyuncPrometheusMonitorApplication.class, args);
    }

}
