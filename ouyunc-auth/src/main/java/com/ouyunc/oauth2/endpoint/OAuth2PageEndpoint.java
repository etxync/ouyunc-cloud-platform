package com.ouyunc.oauth2.endpoint;

import com.ouyunc.common.constant.Auth2Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Author fangzhenxun
 * @Description 自定义oauth2的授权码登录和确认认证的页面控制层 ，
 * ，注意：自定义授权页面，一定要在类上加@SessionAttributes({"authorizationRequest"})
 * @Date 2020/4/26 11:26
 **/
@Controller
@SessionAttributes({"authorizationRequest"})
public class OAuth2PageEndpoint {




    /**
     * @Author fangzhenxun
     * @Description  自定义授权跳转登录页面之后的错误页面，这里可以跳页面也可以序列化返回json,看业务需求
     * @Date 2020/4/26 12:15
     * @return org.springframework.web.servlet.ModelAndView
     **/
    @RequestMapping(Auth2Constant.OAUTH2_ERROR)
    public ModelAndView oauthError(HttpServletRequest request) {
        ModelAndView view = new ModelAndView();
        view.addObject(Auth2Constant.CLIENT_ID, request.getParameter(Auth2Constant.CLIENT_ID));
        view.setViewName("base-error");
        return view;
    }



    /**
     * @Author fangzhenxun
     * @Description  授权码登录页面,
     * 1，如用户名密码错误(需要将/login?error 重写覆盖掉 在webSecurityConfig 配置) 会重定向到该页码进行提示
     * 2，这里值的注意的是表单的用户名name和password输入框的name=""要和security里面的验证的对应:
     * name="username";name="password",否则无法识别,另外action="/authentication/form"要与认证服务器中  endpoint.loginProcessingUrl("/authentication/form")相对应,原因为:
     * 由于security是由UsernamePasswordAuthenticationFilter这个类定义登录的,里面默认是/login路径,我们要让他用我们的/authentication/form路径,就需要配置.loginProcessingUrl("/authentication/form")
     * 注意：如果是不能重定向到登录页面，可能是session为null
     * @Date 2020/4/26 11:43
     * @param model
     * @return java.lang.String
     **/
    @RequestMapping(Auth2Constant.OAUTH2_LOGIN)
    public String loginPage(Model model, HttpServletRequest request){
        // 取出重定向所传递的参数
        String clientId = request.getParameter(Auth2Constant.CLIENT_ID);
        String errorMsg = request.getParameter(Auth2Constant.LOGIN_ERROR_MSG);
        if (StringUtils.isNotBlank(errorMsg)) {
            model.addAttribute(Auth2Constant.LOGIN_ERROR_MSG, errorMsg);
        }
        model.addAttribute(Auth2Constant.LOGIN_PROCESS_URL_KEY, Auth2Constant.LOGIN_PROCESS_URL);
        model.addAttribute(Auth2Constant.FORM_LOGIN_TYPE, Auth2Constant.FORM_LOGIN_TYPE_USERNAME_PASSWORD);
        model.addAttribute(Auth2Constant.CLIENT_ID, clientId);
        return "base-login";
    }


    /**
     * @Author fangzhenxun
     * @Description  自定义授权页面，如果登录成功后没有跳转授权页面定位这个类  ApprovalStoreUserApprovalHandler
     * 可以设置自动授权（登录成功后不需要跳到授权页面，直接携带code重定向到配置的url）,只需要在oauth_client_details 表的autoApprove字段设置true
     * @Date 2020/4/26 12:15
     * @param model
     * @return org.springframework.web.servlet.ModelAndView
     **/
    @RequestMapping(Auth2Constant.OAUTH2_CONFIRM_ACCESS)
    public ModelAndView getAccessConfirmation(Map<String, Object> model) {
        AuthorizationRequest authorizationRequest = (AuthorizationRequest) model.get("authorizationRequest");
        ModelAndView view = new ModelAndView();
        view.addObject(Auth2Constant.CLIENT_ID, authorizationRequest.getClientId());
        view.setViewName("base-grant");
        return view;
    }




}
