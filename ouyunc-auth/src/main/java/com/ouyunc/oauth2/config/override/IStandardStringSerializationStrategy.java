package com.ouyunc.oauth2.config.override;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.security.oauth2.provider.token.store.redis.StandardStringSerializationStrategy;

/**
 * @Author fangzhenxun
 * @Description: oauth2 redis 值的序列化的策略，键的序列化与反序列化在父类已实现,
 * @todo 这里实现起来太复杂，暂时不处理
 * @Version V1.0
 **/
@Deprecated
public class IStandardStringSerializationStrategy extends StandardStringSerializationStrategy {


    Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);


    public IStandardStringSerializationStrategy() {
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
    }


    /**
     * @Author fangzhenxun
     * @Description redis 值的反序列化
     * @param bytes
     * @param clazz
     * @return T
     */
    @Override
    protected <T> T deserializeInternal(byte[] bytes, Class<T> clazz) {
        return (T) jackson2JsonRedisSerializer.deserialize(bytes);
    }

    /**
     * @Author fangzhenxun
     * @Description redis 值的序列化
     * @param object
     * @return byte[]
     */
    @Override
    protected byte[] serializeInternal(Object object) {
        return jackson2JsonRedisSerializer.serialize(object);
    }


}
