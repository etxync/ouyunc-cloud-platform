package com.ouyunc.oauth2.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(prefix = "oauth2.token.dynamic-store")
public class TokenDynamicRedisPrefixProperties {
    /**
     * token 动态redis 前缀
     */
    private Map<String, String> redisPrefix;

    public Map<String, String> getRedisPrefix() {
        return redisPrefix;
    }

    public void setRedisPrefix(Map<String, String> redisPrefix) {
        this.redisPrefix = redisPrefix;
    }
}
