package com.ouyunc.oauth2.config.override;

import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author fangzhenxun
 * @Description 自定义认证详情资源接口，该接口用于在Spring Security登录过程中对用户的登录信息的详细信息进行填充，
 * @Date 2020/4/28 17:34
 **/
@Component
public class IAuthenticationDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, IWebAuthenticationDetails> {

    /**
     * @Author fangzhenxun
     * @Description  重写覆盖WebAuthenticationDetails的生成自定义的IWebAuthenticationDetails
     * @Date 2020/4/28 17:36
     * @param context
     * @return com.xyt.auth2.config.override.IWebAuthenticationDetails
     **/
    @Override
    public IWebAuthenticationDetails buildDetails(HttpServletRequest context) {
        return new IWebAuthenticationDetails(context);
    }
}
