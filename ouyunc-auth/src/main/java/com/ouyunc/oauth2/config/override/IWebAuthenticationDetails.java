package com.ouyunc.oauth2.config.override;

import com.ouyunc.common.constant.Auth2Constant;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author fangzhenxun
 * @Description 授权码认证时，自定义WebAuthenticationDetails 为了使用表单登录验证时，附带增加额外的数据，如验证码、用户类型等
 * @Date 2020/4/28 17:17
 **/

public class IWebAuthenticationDetails extends WebAuthenticationDetails {

    private static final long serialVersionUID = 6975601077710753878L;

    /**
     * 登录类型
     **/
    private final String loginType;


    /**
     * Records the remote address and will also set the session Id if a session already
     * exists (it won't create one).
     *
     * @param request that the authentication request was received from
     */
    public IWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        //从请求参数中获取额外参数，如登陆类型：用户名密码登陆，手机号验证码登陆等
        loginType = request.getParameter(Auth2Constant.FORM_LOGIN_TYPE);
    }

    public String getLoginType() {
        return loginType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString()).append("; loginType: ").append(this.getLoginType());
        return sb.toString();
    }
}
