package com.ouyunc.oauth2.config.override;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ouyunc.common.constant.Auth2Constant;
import com.ouyunc.common.constant.enums.ResponseCodeEnum;
import com.ouyunc.common.base.ResponseResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.*;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.RedirectUrlBuilder;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author fangzhenxun
 * @Description 自定义没有权限时的表单登录成功后的处理逻辑,动态实现根据条件在运行时不同的跳转登录页面
 * @Date 2020/5/6 13:46
 **/
public class ILoginUrlAuthenticationEntryPoint implements AuthenticationEntryPoint,InitializingBean {

    // ~ Static fields/initializers
    // =====================================================================================

    private static final Log logger = LogFactory
            .getLog(LoginUrlAuthenticationEntryPoint.class);

    // ~ Instance fields
    // ================================================================================================

    private PortMapper portMapper = new PortMapperImpl();

    private PortResolver portResolver = new PortResolverImpl();

    private String loginFormUrl = Auth2Constant.OAUTH2_LOGIN;

    private boolean forceHttps = false;

    private boolean useForward = false;

    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    /**
     *
     * @param loginFormUrl URL where the login page can be found. Should either be
     * relative to the web-app context path (include a leading {@code /}) or an absolute
     * URL.
     */
    public ILoginUrlAuthenticationEntryPoint(String loginFormUrl) {
        Assert.notNull(loginFormUrl, "loginFormUrl cannot be null");
        this.loginFormUrl = loginFormUrl;
    }

    public ILoginUrlAuthenticationEntryPoint() {
    }

    // ~ Methods
    // ========================================================================================================

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.isTrue(
                StringUtils.hasText(loginFormUrl)
                        && UrlUtils.isValidRedirectUrl(loginFormUrl),
                "loginFormUrl must be specified and must be a valid redirect URL");
        if (useForward && UrlUtils.isAbsoluteUrl(loginFormUrl)) {
            throw new IllegalArgumentException(
                    "useForward must be false if using an absolute loginFormURL");
        }
        Assert.notNull(portMapper, "portMapper must be specified");
        Assert.notNull(portResolver, "portResolver must be specified");
    }

   /**
    * @Author fangzhenxun
    * @Description  这个方法是获取表单登录时所获取的重定向路径（在不同环境下重定向到不同的页面）
    * @Date 2020/5/6 14:01
    * @param request
    * @param response
    * @param exception
    * @return java.lang.String
    **/
    protected String determineUrlToUseForThisRequest(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        //这里从请求上获取所需的参数可以动态重定向到某个登录页面，如卖家端和买家端页面或者是管理端登录页面以及一些第三方登录
        String client_id = request.getParameter("client_id");
        //System.out.println("client_id" + client_id);
        //这里可以实现动态获取需要，拼接在路径上往下游传递
        String loginFormUrl = getLoginFormUrl();

        return loginFormUrl + "?client_id=" + client_id;
        //return getLoginFormUrl();
    }

    /**
     * @Author fangzhenxun
     * @Description 响应json返回信息，不跳页面
     * @param request
     * @return void
     */
    public boolean responseJson(HttpServletRequest request) throws IOException {
        // 判断是否是处理授权码的跳页面请求，如果不是同一返回json响应数据
        ///会有一个favicon.ico的图标请求
        return !request.getServletPath().equals(Auth2Constant.OAUTH_AUTHORIZE_URL);
    }

    /**
     * Performs the redirect (or forward) to the login form URL.
     * 只这里面是可以取到request 的参数
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,AuthenticationException authException) throws IOException, ServletException {
        // 判断是否需要返回json数据
        if (responseJson(request)) {
            response.setContentType("application/json;charset=UTF-8");
            response.setStatus(ResponseCodeEnum.FORBIDDEN.code());
            response.getWriter().print(JSON.toJSONString(ResponseResult.error(ResponseCodeEnum.FORBIDDEN), SerializerFeature.WriteMapNullValue));
            return;
        }
        // @todo 在这里可以自定义是否需要跳转到登录页面，如果不进行跳转到登录页面可以手动调用登录的接口就行数据流程的走，这样就只会跳转到授权页面
        // 登录页面点击登录后，post请求/login路径，或自定义的路径，按照FilterChainProxy的filter链运行到AbstractAuthenticationProcessingFilter 然后调用 UsernamePasswordAuthenticationFilter，验证通过后执行successHandler.onAuthenticationSuccess(request, response, authResult)，获取session中的savedrequest，重定向到原先的地址/oauth/authorize，并附带完整请求参数。
        // 如果这里不需要跳转到登录页面，可以手动执行 登录的接口进行认证：比如根据某个标识判断不需要登录然后调用http://localhost:8010/authentication/form，传入用户名或密码或其他标识进行登录就ok了
        // do something...

        // 授权码模式，会跳页面
        //String client_id = request.getParameter("client_id");
        String redirectUrl = null;

        //转发的方式
        if (useForward) {
            if (forceHttps && "http".equals(request.getScheme())) {
                redirectUrl = buildHttpsRedirectUrlForRequest(request);
            }
            if (redirectUrl == null) {
                String loginForm = determineUrlToUseForThisRequest(request, response,authException);
                if (logger.isDebugEnabled()) {
                    logger.info("Server side forward to: " + loginForm);
                }
                RequestDispatcher dispatcher = request.getRequestDispatcher(loginForm);
                dispatcher.forward(request, response);
                return;
            }
        } else {
            //重定向的方式
            // redirect to login page. Use https if forceHttps true
            redirectUrl = buildRedirectUrlToLoginPage(request, response, authException);
        }
        redirectStrategy.sendRedirect(request, response, redirectUrl);
    }

    protected String buildRedirectUrlToLoginPage(HttpServletRequest request,
                                                 HttpServletResponse response, AuthenticationException authException) {

        String loginForm = determineUrlToUseForThisRequest(request, response,
                authException);

        if (UrlUtils.isAbsoluteUrl(loginForm)) {
            return loginForm;
        }

        int serverPort = portResolver.getServerPort(request);
        String scheme = request.getScheme();

        RedirectUrlBuilder urlBuilder = new RedirectUrlBuilder();

        urlBuilder.setScheme(scheme);
        urlBuilder.setServerName(request.getServerName());
        urlBuilder.setPort(serverPort);
        urlBuilder.setContextPath(request.getContextPath());
        urlBuilder.setPathInfo(loginForm);

        if (forceHttps && "http".equals(scheme)) {
            Integer httpsPort = portMapper.lookupHttpsPort(Integer.valueOf(serverPort));

            if (httpsPort != null) {
                // Overwrite scheme and port in the redirect URL
                urlBuilder.setScheme("https");
                urlBuilder.setPort(httpsPort.intValue());
            }
            else {
                logger.warn("Unable to redirect to HTTPS as no port mapping found for HTTP port "
                        + serverPort);
            }
        }

        return urlBuilder.getUrl();
    }

    /**
     * Builds a URL to redirect the supplied request to HTTPS. Used to redirect the
     * current request to HTTPS, before doing a forward to the login page.
     */
    protected String buildHttpsRedirectUrlForRequest(HttpServletRequest request)
            throws IOException, ServletException {

        int serverPort = portResolver.getServerPort(request);
        Integer httpsPort = portMapper.lookupHttpsPort(Integer.valueOf(serverPort));

        if (httpsPort != null) {
            RedirectUrlBuilder urlBuilder = new RedirectUrlBuilder();
            urlBuilder.setScheme("https");
            urlBuilder.setServerName(request.getServerName());
            urlBuilder.setPort(httpsPort.intValue());
            urlBuilder.setContextPath(request.getContextPath());
            urlBuilder.setServletPath(request.getServletPath());
            urlBuilder.setPathInfo(request.getPathInfo());
            urlBuilder.setQuery(request.getQueryString());

            return urlBuilder.getUrl();
        }

        // Fall through to server-side forward with warning message
        logger.warn("Unable to redirect to HTTPS as no port mapping found for HTTP port "
                + serverPort);

        return null;
    }

    /**
     * Set to true to force login form access to be via https. If this value is true (the
     * default is false), and the incoming request for the protected resource which
     * triggered the interceptor was not already <code>https</code>, then the client will
     * first be redirected to an https URL, even if <tt>serverSideRedirect</tt> is set to
     * <tt>true</tt>.
     */
    public void setForceHttps(boolean forceHttps) {
        this.forceHttps = forceHttps;
    }

    protected boolean isForceHttps() {
        return forceHttps;
    }

    public String getLoginFormUrl() {
        return loginFormUrl;
    }

    public void setPortMapper(PortMapper portMapper) {
        Assert.notNull(portMapper, "portMapper cannot be null");
        this.portMapper = portMapper;
    }

    protected PortMapper getPortMapper() {
        return portMapper;
    }

    public void setPortResolver(PortResolver portResolver) {
        Assert.notNull(portResolver, "portResolver cannot be null");
        this.portResolver = portResolver;
    }

    protected PortResolver getPortResolver() {
        return portResolver;
    }

    /**
     * Tells if we are to do a forward to the {@code loginFormUrl} using the
     * {@code RequestDispatcher}, instead of a 302 redirect.
     *
     * @param useForward true if a forward to the login page should be used. Must be false
     * (the default) if {@code loginFormUrl} is set to an absolute value.
     */
    public void setUseForward(boolean useForward) {
        this.useForward = useForward;
    }

    protected boolean isUseForward() {
        return useForward;
    }
}
