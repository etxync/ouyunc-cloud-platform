package com.ouyunc.oauth2.feign;


import com.ouyunc.service.api.UserServiceApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author fangzhenxun
 * @date 2019/11/5 17:27
 * @description feign 用户客户端，用来调用用户中心的数据
 */
@FeignClient(value = "user-service")
public interface UserClient extends UserServiceApi {

}
