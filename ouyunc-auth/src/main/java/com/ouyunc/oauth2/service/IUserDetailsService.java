package com.ouyunc.oauth2.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Map;

/**
 * @Author fangzhenxun
 * @Description 自定义userDetailsService 继承
 * @Date 2020/4/21 18:03
 **/
public interface IUserDetailsService extends UserDetailsService {


    /**
     * @Author fangzhenxun
     * @Description  自定义load接口
     * @Date 2020/4/22 10:51
     * @param username 用户名
     * @param extendMap 扩展字段map
     * @return org.springframework.security.core.userdetails.UserDetails
     **/
    UserDetails loadUserByUsername(String username, Map<String, Object> extendMap) throws UsernameNotFoundException;

}
