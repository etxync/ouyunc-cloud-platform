package com.ouyunc.oauth2.service;

/**
 * @Author fangzhenxun
 * @Description token相关操作
 * @Date 2020/7/8 18:04
 **/
public interface IConsumerTokenServices {

    /**
     * @Author fangzhenxun
     * @Description  吊销token凭证（退出登录）
     * @Date 2020/7/8 18:04
     * @param tokenValue
     * @return boolean
     **/
    boolean revokeToken(String tokenValue);
}
