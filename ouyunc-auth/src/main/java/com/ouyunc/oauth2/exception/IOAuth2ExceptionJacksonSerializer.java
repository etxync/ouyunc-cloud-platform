package com.ouyunc.oauth2.exception;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.ouyunc.common.base.ResponseResult;
import org.springframework.web.util.HtmlUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Map;

/**
 * @Author fangzhenxun
 * @Description 自定义IOAuth2Exception的 异常序列化类
 * @Date 2020/4/23 9:27
 **/
public class IOAuth2ExceptionJacksonSerializer extends StdSerializer<IOAuth2Exception> {

    protected IOAuth2ExceptionJacksonSerializer() {
        super(IOAuth2Exception.class);
    }


    /**
     * @Author fangzhenxun
     * @Description  这里需要处理对自定义异常
     * @Date 2020/4/23 9:36
     * @param oauth2Exception 被序列化的数据
     * @param jsonGenerator 主要是用来生成Json格式的内容的
     * @param serializerProvider 序列化提供者
     * @return void
     **/
    @Override
    public void serialize(IOAuth2Exception oauth2Exception, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        //设置message
        String errorMessage = oauth2Exception.getMessage();
        if (errorMessage != null) {
            //将html格式的异常，转换成text格式（使用符号代替标签）
            errorMessage = HtmlUtils.htmlEscape(errorMessage) ;
        }
        //写json格式
        jsonGenerator.writeStartObject();
        //设置code
        jsonGenerator.writeNumberField(ResponseResult.Fields.code, oauth2Exception.getHttpErrorCode());

        jsonGenerator.writeStringField(ResponseResult.Fields.message, errorMessage);

        //设置data
        jsonGenerator.writeObjectField(ResponseResult.Fields.data, null);

        jsonGenerator.writeBooleanField(ResponseResult.Fields.success, true);
        //设置
        jsonGenerator.writeNumberField(ResponseResult.Fields.timestamp, LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli());

        //添加，自定义信息
        if (oauth2Exception.getAdditionalInformation()!=null) {
            for (Map.Entry<String, String> entry : oauth2Exception.getAdditionalInformation().entrySet()) {
                jsonGenerator.writeStringField(entry.getKey(), entry.getValue());
            }
        }
        //结束写json
        jsonGenerator.writeEndObject();
    }
}
