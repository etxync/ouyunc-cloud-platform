package com.ouyunc.oauth2.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * @Author fangzhenxun
 * @Description 自定义oauth2异常
 * @Date 2020/4/22 17:32
 **/
@JsonSerialize(using = IOAuth2ExceptionJacksonSerializer.class)
public class IOAuth2Exception extends OAuth2Exception {
    private int code;

    public IOAuth2Exception(String msg) {
        super(msg);
    }

    @Override
    public int getHttpErrorCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
