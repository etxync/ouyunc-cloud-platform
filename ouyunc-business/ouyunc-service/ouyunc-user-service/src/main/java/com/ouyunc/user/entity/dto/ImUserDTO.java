package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @author fzx
 */
@ApiModel("im 用户注册dto")
@Data
public class ImUserDTO implements Serializable {


    /**
     * 用户主键
     */
    private String id;

    /**
     * 开放id
     */
    private String openId;

    /**
     * 用户名称（对应于身份证）
     */
    private String username;

    /**
     * 用户名密码
     */
    private String password;

    /**
     * 用户别名
     */
    private String nickName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号（国内）
     */
    private String phoneNum;

    /**
     * 身份证号码
     */
    private String idCardNum;


    /**
     * 用户头像url
     */
    private String avatar;

    /**
     * 座右铭/格言
     */
    private String motto;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别：0-女，1-男，2-其他
     */
    private Integer sex;

    /**
     * 用户状态：0-正常，1-异常（被平台封禁）
     */
    private Integer status;
}
