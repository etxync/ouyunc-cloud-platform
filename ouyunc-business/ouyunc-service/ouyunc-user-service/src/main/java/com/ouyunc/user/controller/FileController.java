package com.ouyunc.user.controller;

import com.alibaba.fastjson2.JSON;
import com.ouyunc.common.base.ResponseResult;
import com.ouyunc.user.entity.dto.ImUserDTO;
import com.ouyunc.user.service.ImFileService;
import com.ouyunc.web.annotation.CurrentLoginUser;
import com.ouyunc.web.common.LoginUser;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Api(tags = "文件相关api")
@RestController
@RequestMapping("/file")
public class FileController {

    @Resource
    private ImFileService imFileService;

    @ApiOperation("上传文件")
    @ApiImplicitParams({@ApiImplicitParam(name = "fileType", value = "文件类型: 1-语音",dataType = "int", required = true, paramType = "query"),@ApiImplicitParam(name = "file", value = "文件流对象,接收数组格式", required = true,dataType = "MultipartFile",allowMultiple = true)})
    @PostMapping("/upload")
    public ResponseResult upload(@RequestParam("fileType") Integer fileType, @RequestParam("file") MultipartFile file, @CurrentLoginUser LoginUser loginUser) {
        Object principle = loginUser.principle();
        String userId = JSON.parseObject(JSON.toJSONString(principle)).getJSONObject("userDetail").getString("id");
        return ResponseResult.success(imFileService.upload(fileType, file, userId));
    }
}
