package com.ouyunc.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ouyunc.im.domain.ImBlacklist;
import com.ouyunc.user.mapper.ImBlacklistMapper;
import com.ouyunc.user.service.ImBlacklistService;
import org.springframework.stereotype.Service;

@Service
public class ImBlacklistServiceImpl extends ServiceImpl<ImBlacklistMapper, ImBlacklist> implements ImBlacklistService {
}
