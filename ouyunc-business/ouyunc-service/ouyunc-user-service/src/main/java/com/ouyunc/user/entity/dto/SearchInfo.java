package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author fzx
 */
@ApiModel("搜索信息")
@Data
public class SearchInfo {

    /**
     * 用户或群组id
     */
    private String id;


    /**
     * 显示名称
     */
    private String nickName;


    /**
     * 头像集合或字符串Url
     */
    private Object avatar;



    public SearchInfo() {
    }

    public SearchInfo(String id, String nickName, Object avatar) {
        this.id = id;
        this.nickName = nickName;
        this.avatar = avatar;
    }
}
