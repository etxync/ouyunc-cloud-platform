package com.ouyunc.user.controller;

import com.ouyunc.service.api.UserServiceApi;
import com.ouyunc.service.dto.OauthClientDetailsDTO;
import com.ouyunc.service.dto.UserDTO;
import com.ouyunc.user.service.ImUserService;
import com.ouyunc.user.service.OauthClientDetailsService;
import com.ouyunc.common.base.ResponseResult;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "oauth2 客户端相关 API")
@RestController
public class OauthClientDetailsController implements UserServiceApi {

    @Resource
    private OauthClientDetailsService oauthClientDetailsService;

    @Resource
    private ImUserService imUserService;

    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    @Override
    public ResponseResult<UserDTO> getUserByUserName(String username) {
        return ResponseResult.success(imUserService.getUserByUserName(username));
    }

    /**
     * 根据客户端id 查询oauth2 客户端信息
     * @param clientId
     * @return
     */
    @Override
    public ResponseResult<OauthClientDetailsDTO> loadClientByClientId(String clientId) {
        return ResponseResult.success(oauthClientDetailsService.loadClientByClientId(clientId));
    }
}
