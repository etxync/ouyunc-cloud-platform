package com.ouyunc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ouyunc.im.domain.ImFriend;
import org.springframework.stereotype.Repository;

@Repository
public interface ImFriendMapper extends BaseMapper<ImFriend> {


}
