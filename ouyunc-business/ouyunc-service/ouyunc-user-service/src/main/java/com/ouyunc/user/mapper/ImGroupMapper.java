package com.ouyunc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ouyunc.im.domain.ImGroup;
import com.ouyunc.user.entity.dto.ImGroupDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author fzx
 */
@Repository
public interface ImGroupMapper extends BaseMapper<ImGroup> {
    @Select("SELECT \n" +
            "ouyunc_im_group.id, ouyunc_im_group.group_name, ouyunc_im_group_user.group_nick_name, ouyunc_im_group.group_avatar\n" +
            "FROM \n" +
            "ouyunc_im_group \n" +
            "INNER JOIN ouyunc_im_group_user ON ouyunc_im_group.id = ouyunc_im_group_user.group_id \n" +
            "WHERE \n" +
            "ouyunc_im_group.deleted = 0 \n" +
            "AND ouyunc_im_group_user.user_id = #{userId}")
    List<ImGroupDTO> groupList(@Param("userId") String userId);

    @Select("SELECT \n" +
            "ouyunc_im_group.id, ouyunc_im_group.group_name, ouyunc_im_group_user.group_nick_name, ouyunc_im_group.group_avatar\n" +
            "FROM \n" +
            "ouyunc_im_group \n" +
            "INNER JOIN ouyunc_im_group_user ON ouyunc_im_group.id = ouyunc_im_group_user.group_id \n" +
            "WHERE \n" +
            "ouyunc_im_group.deleted = 0 \n" +
            "AND ouyunc_im_group_user.user_id = #{userId} AND (ouyunc_im_group_user.is_leader = 1 or ouyunc_im_group_user.is_manager = 1) ")
    List<ImGroupDTO> groupListByLeaderOrManager(@Param("userId") String userId);
}
