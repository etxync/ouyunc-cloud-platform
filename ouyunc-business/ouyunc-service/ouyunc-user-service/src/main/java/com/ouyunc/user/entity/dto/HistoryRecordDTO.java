package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 历史记录
 */
@ApiModel("历史记录")
@Data
public class HistoryRecordDTO<T> {

    /**
     * 消息id
     */
    @ApiModelProperty("消息id")
    private String id;


    /**
     * 消息类型：1-系统消息，2-用户消息(私聊，群聊)
     */
    @ApiModelProperty("消息类型：1-系统消息，2-用户消息")
    private Integer type;



    /**
     * 消息内容类型,51-普通文本+表情，55-图片, 56-音频, 57-视频, 54-语音, 58-音视通话记录, 59-视频通话记录
     */
    @ApiModelProperty("消息内容类型,51-普通文本+表情，55-图片, 56-音频, 57-视频, 54-语音, 58-音视通话记录, 59-视频通话记录")
    private Integer contentType;


    /**
     * 消息内容，不同的内容类型，对应不同的内容格式：具体格式见上面的注释
     */
    @ApiModelProperty("消息内容，不同的内容类型，对应不同的内容格式：具体格式见上面的注释")
    private T content;


    /**
     * 消息发送时间戳
     */
    @ApiModelProperty("消息发送时间戳")
    private Long timestamp;

    /**
     * 消息发送者信息
     */
    @ApiModelProperty("消息发送者信息")
    private ImContactDTO fromUser;

    public HistoryRecordDTO() {
    }

    public HistoryRecordDTO(String id, Integer type, Integer contentType, T content, Long timestamp, ImContactDTO fromUser) {
        this.id = id;
        this.type = type;
        this.contentType = contentType;
        this.content = content;
        this.timestamp = timestamp;
        this.fromUser = fromUser;
    }
}
