package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

@ApiModel("im 通知dto")
@Data
public class NotifyDTO implements Serializable {

    /**
     * 用户id（需要显示的用户或群id）
     */
    private String id;


    /**
     * 用户名称
     */
    private String nickName;


    /**
     * 用户头像Url
     */
    private String avatar;


    /**
     * 群id
     */
    private String groupId;

    /**
     * 群昵称
     */
    private String groupNickName;


    /**
     * 群头像Url
     */
    private String groupAvatar;

    /**
     * 申请人id（申请添加好友的人）
     */
    private String applyUserId;

    /**
     * 申请人姓名（申请添加好友的人）
     */
    private String applyUserName;


    /**
     * 邀请人id
     */
    private String inviteUserId;



    /**
     * 邀请人姓名
     */
    private String inviteUserName;


    /**
     * 会话Id (用于区分整个流程会话的)
     */
    private String sessionId;

    /**
     * 被邀请人id
     */
    private String invitedUserId;



    /**
     * 被邀请人姓名
     */
    private String invitedUserName;


    /**
     * 处理人id
     */
    private String handlerUserId;

    /**
     * 处理人姓名/昵称（如果是好友就显示昵称，如果不是好友就显示名称）
     */
    private String handlerUsername;


    /**
     * 处理状态: 0-待验证，1-已同意（同意），2-被拒绝（拒绝）,3-待通过（如果是群成员邀请自己，自己通过后状态会是待通过）
     */
    private Integer handlerStatus;

    /**
     * 用户类型：1-个人，2-群
     */
    private Integer userType;


    /**
     * 消息内容类型
     */
    private Integer messageContentType;

    /**
     * 最有一条消息文本内容
     */
    private String lastMsgContent;


    /**
     * 消息发送时间戳
     */
    private Long createTime;

    public NotifyDTO() {
    }

    public NotifyDTO(String id, Integer userType, String applyUserId, String lastMsgContent, Integer handlerStatus, Long createTime) {
        this.id = id;
        this.userType = userType;
        this.applyUserId = applyUserId;
        this.lastMsgContent = lastMsgContent;
        this.handlerStatus = handlerStatus;
        this.createTime = createTime;
    }

    public NotifyDTO(String id, String groupId, String applyUserId, Integer userType, String lastMsgContent, Integer handlerStatus, String handlerUserId, String inviteUserId, Integer messageContentType,  Long createTime) {
        this.id = id;
        this.groupId = groupId;
        this.applyUserId = applyUserId;
        this.userType = userType;
        this.lastMsgContent = lastMsgContent;
        this.handlerStatus = handlerStatus;
        this.handlerUserId = handlerUserId;
        this.inviteUserId = inviteUserId;
        this.messageContentType = messageContentType;
        this.createTime = createTime;
    }


    public NotifyDTO(String id, String groupId, String applyUserId, String inviteUserId,String invitedUserId, String sessionId, String handlerUserId, Integer handlerStatus, Integer userType, Integer messageContentType, String lastMsgContent, Long createTime) {
        this.id = id;
        this.groupId = groupId;
        this.applyUserId = applyUserId;
        this.inviteUserId = inviteUserId;
        this.invitedUserId = invitedUserId;
        this.sessionId = sessionId;
        this.handlerUserId = handlerUserId;
        this.handlerStatus = handlerStatus;
        this.userType = userType;
        this.messageContentType = messageContentType;
        this.lastMsgContent = lastMsgContent;
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (!(o instanceof NotifyDTO)) {return false;}
        NotifyDTO notifyDTO = (NotifyDTO) o;
        return id.equals(notifyDTO.id) && groupId.equals(notifyDTO.groupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, groupId);
    }
}
