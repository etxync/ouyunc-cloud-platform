package com.ouyunc.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ouyunc.file.plugins.base.FileReturn;
import com.ouyunc.user.entity.ImFile;
import org.springframework.web.multipart.MultipartFile;

public interface ImFileService extends IService<ImFile> {
    FileReturn upload(Integer fileType, MultipartFile file, String userId);
}
