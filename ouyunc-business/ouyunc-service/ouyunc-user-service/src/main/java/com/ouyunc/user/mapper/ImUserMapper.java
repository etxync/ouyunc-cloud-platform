package com.ouyunc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ouyunc.im.domain.ImUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author fzx
 */
@Repository
public interface ImUserMapper extends BaseMapper<ImUser> {

    @Select("SELECT ouyunc_im_user.* FROM ouyunc_im_user left join ouyunc_im_friend on ouyunc_im_user.id = ouyunc_im_friend.friend_user_id WHERE ouyunc_im_friend.user_id = #{userId}  and ouyunc_im_user.deleted = 0")
    List<ImUser> contact(@Param("userId") String userId);

}
