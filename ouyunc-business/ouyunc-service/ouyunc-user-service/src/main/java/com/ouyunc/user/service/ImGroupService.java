package com.ouyunc.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ouyunc.im.domain.ImGroup;
import com.ouyunc.user.entity.dto.ImUserDetailDTO;
import com.ouyunc.user.entity.dto.SearchInfo;

import java.util.List;

/**
 * @author fzx
 */
public interface ImGroupService extends IService<ImGroup> {
    List<SearchInfo> search(String username);

    ImUserDetailDTO getGroup(String userId, String groupId);


    ImGroup createGroup(String userId, ImGroup imGroup);

    int cancelGroupManager(String groupId, String originManagerUserId,Integer isManager);
}
