package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
/**
 * im 消息DTo
 */
@ApiModel(" im 消息DTo")
@Data
public class ImMessageDTO implements Serializable {

    /**
     * 消息id，
     */
    @ApiModelProperty("消息id")
    private String id;


    /**
     * 消息发送者昵称
     */
    @ApiModelProperty("消息发送者昵称")
    private String from;

    /**
     * 消息内容类型
     */
    @ApiModelProperty("消息内容类型")
    private Integer contentType;

    /**
     * 消息内容
     */
    @ApiModelProperty("消息内容")
    private String content;


    /**
     * 消息发送时间
     */
    @ApiModelProperty("消息发送时间")
    private Long createTime;
}
