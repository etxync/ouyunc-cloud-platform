package com.ouyunc.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ouyunc.db.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("文件相关")
@TableName("ouyunc_im_file")
public class ImFile extends BaseEntity implements Serializable {

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;


    /**
     * 文件名称，包含后缀名
     */
    private String fileName;


    /**
     * 文件原始名称
     */
    private String fileOriginName;

    /**
     * 文件网络访问完整路径：http(s)://xx
     */
    private String fileUrl;


    /**
     * 文件类型，标识是哪种业务类型的文件：1-语音文件
     */
    private String filePath;


    /**
     * 文件的md5
     */
    private String fileMd5;


    /**
     * 文件后缀名
     */
    private String fileSuffix;


    /**
     * 文件关联id
     */
    private String relationId;


    public ImFile() {
    }

    public ImFile(String fileName, String fileOriginName, String fileUrl, String filePath, String fileMd5, String fileSuffix, String relationId) {
        this.fileName = fileName;
        this.fileOriginName = fileOriginName;
        this.fileUrl = fileUrl;
        this.filePath = filePath;
        this.fileMd5 = fileMd5;
        this.fileSuffix = fileSuffix;
        this.relationId = relationId;
    }
}
