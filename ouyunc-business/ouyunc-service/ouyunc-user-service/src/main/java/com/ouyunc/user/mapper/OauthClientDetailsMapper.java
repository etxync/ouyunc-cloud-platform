package com.ouyunc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ouyunc.user.entity.OauthClientDetails;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author fzx
 */
@Repository
public interface OauthClientDetailsMapper extends BaseMapper<OauthClientDetails> {
}
