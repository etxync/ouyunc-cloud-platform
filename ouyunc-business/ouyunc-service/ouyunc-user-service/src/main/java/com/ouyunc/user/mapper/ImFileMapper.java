package com.ouyunc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ouyunc.user.entity.ImFile;
import org.springframework.stereotype.Repository;

@Repository
public interface ImFileMapper extends BaseMapper<ImFile> {
}
