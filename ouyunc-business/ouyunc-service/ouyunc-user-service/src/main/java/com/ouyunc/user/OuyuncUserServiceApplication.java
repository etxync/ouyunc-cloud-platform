package com.ouyunc.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@MapperScan(basePackages = {"com.ouyunc.user.mapper"})
@SpringBootApplication
public class OuyuncUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OuyuncUserServiceApplication.class, args);
    }

}
