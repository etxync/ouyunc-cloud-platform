package com.ouyunc.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ouyunc.service.dto.OauthClientDetailsDTO;
import com.ouyunc.user.entity.OauthClientDetails;

public interface OauthClientDetailsService extends IService<OauthClientDetails> {
   OauthClientDetailsDTO loadClientByClientId(String clientId);
}
