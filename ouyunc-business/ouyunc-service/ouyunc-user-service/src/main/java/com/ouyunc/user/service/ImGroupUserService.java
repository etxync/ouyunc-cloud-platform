package com.ouyunc.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ouyunc.im.domain.ImGroupUser;

public interface ImGroupUserService extends IService<ImGroupUser> {
}
