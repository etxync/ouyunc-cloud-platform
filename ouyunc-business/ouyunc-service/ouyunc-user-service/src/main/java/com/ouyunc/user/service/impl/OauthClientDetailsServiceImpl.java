package com.ouyunc.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ouyunc.service.dto.OauthClientDetailsDTO;
import com.ouyunc.user.entity.OauthClientDetails;
import com.ouyunc.user.mapper.OauthClientDetailsMapper;
import com.ouyunc.user.service.OauthClientDetailsService;
import com.ouyunc.web.exception.CustomerException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OauthClientDetailsServiceImpl extends ServiceImpl<OauthClientDetailsMapper, OauthClientDetails> implements OauthClientDetailsService {

    @Autowired
    private OauthClientDetailsMapper oauthClientDetailsMapper;

    @Override
    public OauthClientDetailsDTO loadClientByClientId(String clientId) {
        OauthClientDetails oauthClientDetails = oauthClientDetailsMapper.selectById(clientId);
        return new OauthClientDetailsDTO(oauthClientDetails.getClientId(), oauthClientDetails.getClientSecret(), oauthClientDetails.getResourceIds(), oauthClientDetails.getScope(), oauthClientDetails.getAuthorizedGrantTypes(), oauthClientDetails.getWebServerRedirectUri(), oauthClientDetails.getAuthorities(), oauthClientDetails.getAccessTokenValidity(), oauthClientDetails.getRefreshTokenValidity(),oauthClientDetails.getAdditionalInformation(), oauthClientDetails.getAutoapprove());
    }
}
