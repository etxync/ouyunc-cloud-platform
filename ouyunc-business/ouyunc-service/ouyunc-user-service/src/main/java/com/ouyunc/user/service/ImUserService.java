package com.ouyunc.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ouyunc.im.domain.ImUser;
import com.ouyunc.service.dto.UserDTO;
import com.ouyunc.user.entity.dto.*;

import java.util.List;

/**
 * @author fzx
 */
public interface ImUserService extends IService<ImUser> {
    void register(ImUserDTO imUser);

    UserDTO getUserByUserName(String username);

    List<SearchInfo> search(String username);

    List<ImContactDTO> contactList(String userId);

    List<ImGroupDTO> groupList(String userId);

    ImUserDetailDTO getUser(String userId);

    List<NotifyDTO> newFriendsNotifyList(String userId);

    List<NotifyDTO> groupNotifyList(String userId);

    boolean existFriend(String userId, String to);

    int shieldFriend(String userId, String friendId, Integer isShield);

    int userJoinBlacklist(String loginUserId, String userId, Integer join);

    int deleteFriend(String userId, String friendId);

    ImFriendDTO getFriend(String userId, String friendId);

    boolean existGroup(String identity, String groupId);

    boolean sessionHandler(String sessionId, String groupId);

    List<ImContactDTO> groupMembers(String groupId);

    int updateNickName(String identity, String nicknameRemark, Integer userType,Integer type, String userId);

    List<ImTempChatContactDTO> tempChatList(String userId);

    boolean addTempChat(String from, String to, Integer userType);

    List<ImTempChatContactDTO> contactOnline(String userId);

    IPage<HistoryRecordDTO> chatHistoryRecord(String userId, String identity, Integer userType, Integer pageNum, Integer pageSize,Integer total);

    boolean deleteTempChat(String userId, String identity);

    ImContactDTO groupMember(String groupId, String userId);

    OfflineContent pullOfflineMessage(String userId, OfflineContent offlineContent);

    List<OfflineContent> pullOfflineUnreadMessage(String userId);
}
