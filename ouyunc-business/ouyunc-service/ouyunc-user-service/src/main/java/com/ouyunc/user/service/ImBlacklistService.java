package com.ouyunc.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ouyunc.im.domain.ImBlacklist;

public interface ImBlacklistService extends IService<ImBlacklist> {
}
