package com.ouyunc.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ouyunc.im.domain.ImFriend;
import com.ouyunc.user.mapper.ImFriendMapper;
import com.ouyunc.user.service.ImFriendService;
import org.springframework.stereotype.Service;

@Service
public class ImFriendServiceImpl extends ServiceImpl<ImFriendMapper, ImFriend> implements ImFriendService {
}
