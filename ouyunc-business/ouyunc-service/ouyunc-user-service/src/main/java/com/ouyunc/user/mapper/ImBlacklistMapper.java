package com.ouyunc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ouyunc.im.domain.ImBlacklist;
import org.springframework.stereotype.Repository;

@Repository
public interface ImBlacklistMapper extends BaseMapper<ImBlacklist> {
}
