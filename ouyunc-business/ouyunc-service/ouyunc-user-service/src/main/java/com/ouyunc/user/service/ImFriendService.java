package com.ouyunc.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ouyunc.im.domain.ImFriend;

public interface ImFriendService extends IService<ImFriend> {
}
