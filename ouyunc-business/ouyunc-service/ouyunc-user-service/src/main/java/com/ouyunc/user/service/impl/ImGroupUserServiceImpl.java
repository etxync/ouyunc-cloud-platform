package com.ouyunc.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ouyunc.im.domain.ImGroupUser;
import com.ouyunc.user.mapper.ImGroupUserMapper;
import com.ouyunc.user.service.ImGroupUserService;
import org.springframework.stereotype.Service;

@Service
public class ImGroupUserServiceImpl extends ServiceImpl<ImGroupUserMapper, ImGroupUser> implements ImGroupUserService {
}
