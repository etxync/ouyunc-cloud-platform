package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * im 联系人列表
 */
@Data
@ApiModel("im 联系人列表")
public class ImContactDTO implements Serializable {
    /**
     * 联系人主键id
     */
    @ApiModelProperty("联系人主键id")
    private String id;

    /**
     * 用户名称（对应于身份证）
     */
    @ApiModelProperty("用户名称")
    private String username;


    /**
     * 用户别名
     */
    @ApiModelProperty("用户别名")
    private String nickName;


    /**
     * 用户头像
     */
    @ApiModelProperty("用户头像")
    private String avatar;

    /**
     * 是否是管理员，1-是，0-否
     */
    @ApiModelProperty("是否是管理员")
    private Integer isManager;

    /**
     * 是否是群主，1-是，0-否
     */
    @ApiModelProperty("是否是群主")
    private Integer isLeader;


    public ImContactDTO() {
    }

    public ImContactDTO(String id, String username, String nickName, String avatar, Integer isManager, Integer isLeader) {
        this.id = id;
        this.username = username;
        this.nickName = nickName;
        this.avatar = avatar;
        this.isManager = isManager;
        this.isLeader = isLeader;
    }

    public ImContactDTO(String id, String username, String nickName, String avatar) {
        this.id = id;
        this.username = username;
        this.nickName = nickName;
        this.avatar = avatar;
    }
}
