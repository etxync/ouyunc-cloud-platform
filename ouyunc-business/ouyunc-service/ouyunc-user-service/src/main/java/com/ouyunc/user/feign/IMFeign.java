package com.ouyunc.user.feign;

import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "im-service")
public interface IMFeign {

    @ApiOperation("事务测试")
    @PostMapping("/test")
    Object test(@RequestParam("id") String id) ;
}
