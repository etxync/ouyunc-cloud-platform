package com.ouyunc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ouyunc.im.domain.ImGroupUser;
import com.ouyunc.im.domain.bo.ImGroupUserBO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImGroupUserMapper extends BaseMapper<ImGroupUser> {

    @Select("select ouyunc_im_group_user.group_id,ouyunc_im_user.*, ouyunc_im_group_user.group_nick_name ,ouyunc_im_group_user.is_leader,ouyunc_im_group_user.is_manager   from  ouyunc_im_user LEFT JOIN  ouyunc_im_group_user on ouyunc_im_user.id = ouyunc_im_group_user.user_id left join  ouyunc_im_group on ouyunc_im_group_user.group_id =  ouyunc_im_group.id where ouyunc_im_group.deleted = 0 and  ouyunc_im_user.deleted = 0 and ouyunc_im_group.id = #{groupId}")
    List<ImGroupUserBO> getGroupUserList(@Param("groupId") String groupId);


    @Select("select ouyunc_im_group_user.group_id,ouyunc_im_user.*, ouyunc_im_group_user.group_nick_name ,ouyunc_im_group_user.is_leader,ouyunc_im_group_user.is_manager   from  ouyunc_im_user LEFT JOIN  ouyunc_im_group_user on ouyunc_im_user.id = ouyunc_im_group_user.user_id left join  ouyunc_im_group on ouyunc_im_group_user.group_id =  ouyunc_im_group.id where ouyunc_im_group.deleted = 0 and  ouyunc_im_user.deleted = 0 and ouyunc_im_group_user.group_id = #{groupId} and ouyunc_im_group_user.user_id = #{userId} ")
    ImGroupUserBO getGroupUser(@Param("groupId") String groupId, @Param("userId") String userId);
}
