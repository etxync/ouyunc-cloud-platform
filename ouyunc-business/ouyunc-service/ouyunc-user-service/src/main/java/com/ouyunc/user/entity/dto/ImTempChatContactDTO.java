package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel("im临时聊天联系人及消息列表")
@Data
public class ImTempChatContactDTO {

    /**
     * 主键id
     */
    private String id;


    /**
     * 用户名称（对应于身份证）
     */
    private String username;


    /**
     * 用户别名
     */
    private String nickName;


    /**
     * 用户头像url
     */
    private String avatar;

    /**
     * 是否在线，0-离线，1-在线
     */
    private Integer online;

    /**
     * 是否在线，1-个人，2-群组
     */
    private Integer type;

    /**
     * 未读消息数量
     */
    private Integer unreadMsgCount;

    /**
     * im 消息
     */
    private ImMessageDTO lastMsg;

    public ImTempChatContactDTO() {
    }

    public ImTempChatContactDTO(String id, String username, String nickName, String avatar, Integer online, Integer type, Integer unreadMsgCount, ImMessageDTO imMessageDTO) {
        this.id = id;
        this.username = username;
        this.nickName = nickName;
        this.avatar = avatar;
        this.online = online;
        this.type = type;
        this.unreadMsgCount = unreadMsgCount;
        this.lastMsg = imMessageDTO;
    }
}
