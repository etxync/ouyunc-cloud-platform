package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
@ApiModel("im 好友DTO")
@Data
public class ImFriendDTO implements Serializable {

    /**
     * 主键id
     */
    private Long id;


    /**
     * 用户名称（对应于身份证）
     */
    private String username;


    /**
     * 用户别名
     */
    private String nickName;



    /**
     * 用户头像url
     */
    private String avatar;

    /**
     * 座右铭/格言
     */
    private String motto;


    /**
     * 性别：0-女，1-男，2-其他
     */
    private Integer sex;


    /**
     * 是否屏蔽该好友，0-未屏蔽，1-屏蔽
     */
    private Integer isShield;


    /**
     * 是否在线，0-离线，1-在线
     */
    private Integer online;
    /**
     * 是否存在好友关系，0-不存在，1-存在
     */
    private Integer existFriend;

    /**
     * 是否将好友加入黑名单：0-未加入，1-已加入
     */
    private Integer existBlacklist;

    public ImFriendDTO() {
    }

    public ImFriendDTO(Long id, String username, String nickName, String avatar, String motto, Integer sex, Integer online) {
        this.id = id;
        this.username = username;
        this.nickName = nickName;
        this.avatar = avatar;
        this.motto = motto;
        this.sex = sex;
        this.online = online;
    }


}
