package com.ouyunc.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ouyunc.common.constant.enums.FilePluginEnum;
import com.ouyunc.file.plugins.base.FileContextHolder;
import com.ouyunc.file.plugins.base.FileReturn;
import com.ouyunc.file.plugins.base.UploadConfig;
import com.ouyunc.user.entity.ImFile;
import com.ouyunc.user.mapper.ImFileMapper;
import com.ouyunc.user.service.ImFileService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Service
public class ImFileServiceImpl extends ServiceImpl<ImFileMapper, ImFile> implements ImFileService {

    @Resource
    private ImFileMapper imFileMapper;

    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public FileReturn upload(Integer fileType, MultipartFile file, String userId) {
        UploadConfig build = UploadConfig.builder().bucketName("ouyunc01-1318938781").bucketPath(file.getOriginalFilename()).build();
        FileReturn fileReturn = FileContextHolder.uploader(FilePluginEnum.COS).upload(file, build);
        //ImFile imFile = new ImFile(fileReturn.getFileName(), fileReturn.getFileOriginName(), fileReturn.getFileUrl(), fileReturn.getFilePath(), fileReturn.getFileMd5(), fileReturn.getFileSuffix(), userId);
        //imFileMapper.insert(imFile);
        return fileReturn;
    }
}
