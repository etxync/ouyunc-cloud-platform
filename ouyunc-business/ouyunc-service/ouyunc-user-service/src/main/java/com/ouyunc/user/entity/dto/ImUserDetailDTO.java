package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@ApiModel("im 用户/群组详情dto")
@Data
public class ImUserDetailDTO implements Serializable {

    /**
     * 主键id
     */
    private String id;


    /**
     * 用户名称（对应于身份证）
     */
    private String username;


    /**
     * 用户别名
     */
    private String nickName;



    /**
     * 用户头像url
     */
    private String avatar;

    /**
     * 座右铭/格言
     */
    private String motto;


    /**
     * 性别：0-女，1-男，2-其他
     */
    private Integer sex;



    /**
     * 当前用户是否存在群中； 0-不存在，1-存在
     */
    private Integer existGroup;

    /**
     * 判断是否是群主：1-是，0 否
     */
    private Integer isLeader;

    public ImUserDetailDTO() {
    }

    public ImUserDetailDTO(String id, String username, String nickName, String avatar, String motto) {
        this.id = id;
        this.username = username;
        this.nickName = nickName;
        this.avatar = avatar;
        this.motto = motto;
    }

    public ImUserDetailDTO(String id, String username, String nickName, String avatar, String motto, Integer sex) {
        this.id = id;
        this.username = username;
        this.nickName = nickName;
        this.avatar = avatar;
        this.motto = motto;
        this.sex = sex;
    }
}
