package com.ouyunc.user.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
/**
 * im 群组列表
 */
@Data
@ApiModel("im 群组列表")
public class ImGroupDTO implements Serializable {

    /**
     * 群组主键id
     */
    @ApiModelProperty("群组主键id")
    private String id;


    /**
     * 群组真实名字（群主起的）
     */
    @ApiModelProperty("群组真实名字（群主起的）")
    private String groupName;


    /**
     * 群组别名（每个群成员显示的群的名字）
     */
    @ApiModelProperty("群组别名（每个群成员显示的群的名字）")
    private String groupNickName;


    /**
     * 群组头像
     */
    @ApiModelProperty("群组头像")
    private String groupAvatar;

}
