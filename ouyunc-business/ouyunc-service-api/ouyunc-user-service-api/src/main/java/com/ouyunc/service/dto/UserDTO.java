package com.ouyunc.service.dto;

import com.ouyunc.common.base.Desensitization;
import com.ouyunc.common.base.UserDetail;
import com.ouyunc.common.constant.enums.UserDetailEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

/**
 * @author fzx
 */
@ApiModel("用户信息DTO")
@Data
@FieldNameConstants
public class UserDTO implements Desensitization, Serializable {
    private static final long serialVersionUID = 256L;

    /**
     * 主键id,uuid
     **/
    @ApiModelProperty("主键id,雪花id")
    private String id;

    /**
     * 全局唯一ID，使用uuid，方便以后对外开放api使用
     **/
    @ApiModelProperty("全局唯一ID，使用uuid，方便以后对外开放api使用")
    private String openId;

    /**
     * 用户名称
     **/
    @ApiModelProperty("用户名称登录名称")
    private String username;

    /**
     * 真实名称
     **/
    @ApiModelProperty("真实名称")
    private String realName;

    /**
     * 用户别名
     */
    private String nickName;

    /**
     * 密码
     **/
    @ApiModelProperty("密码")
    @UserDetail(fieldName = UserDetailEnum.PASSWORD)
    private String password;

    /**
     * 用户头像链接url
     **/
    @ApiModelProperty("用户头像链接url")
    private String avatar;

    /**
     * 用户座右铭（个性标语）
     **/
    @ApiModelProperty("用户座右铭（个性标语）")
    private String motto;



    /**
     * 性别： 1-男，2-女，3-不确定
     **/
    @ApiModelProperty("性别： 1-男，2-女，3-不确定")
    private Integer sex;

    /**
     * 生日，农历
     **/
    @ApiModelProperty("生日，农历")
    private LocalDate birthday;

    /**
     * 手机号码
     **/
    @ApiModelProperty("手机号码")
    @UserDetail(fieldName = UserDetailEnum.USERNAME)
    private String phoneNum;

    /**
     * 邮箱地址
     **/
    @ApiModelProperty("邮箱地址")
    private String email;

    /**
     * 身份证号码
     **/
    @ApiModelProperty("身份证号码")
    private String idCardNum;


    /**
     * 用户权限字符串set集合，不对应数据库字段
     */
    @ApiModelProperty("用户权限字符串set集合")
    private Set<String> authorities;


    /**
     * 用户状态
     **/
    @ApiModelProperty("用户状态")
    private Integer status;

    public UserDTO() {
    }

    public UserDTO(String id, String openId, String username, String realName,String nickName, String password, String avatar, String motto, Integer sex, LocalDate birthday, String phoneNum, String email, String idCardNum, Set<String> authorities, Integer status) {
        this.id = id;
        this.openId = openId;
        this.username = username;
        this.realName = realName;
        this.nickName = nickName;
        this.password = password;
        this.avatar = avatar;
        this.motto = motto;
        this.sex = sex;
        this.birthday = birthday;
        this.phoneNum = phoneNum;
        this.email = email;
        this.idCardNum = idCardNum;
        this.authorities = authorities;
        this.status = status;
    }

    @Override
    public Object desensitization() {
        // 不让别人看到密码
        this.setPassword(null);
        this.setIdCardNum(null);
        return this;
    }
}
