package com.ouyunc.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.common"})
@Configuration
public class OuyuncUserServiceApiApplication {
}
