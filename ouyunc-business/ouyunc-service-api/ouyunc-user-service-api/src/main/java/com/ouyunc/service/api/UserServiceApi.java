package com.ouyunc.service.api;

import com.ouyunc.service.dto.OauthClientDetailsDTO;
import com.ouyunc.service.dto.UserDTO;
import com.ouyunc.common.base.ResponseResult;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Author fangzhenxun
 * @Description: 用户open feign api
 * @Version V1.0
 **/
public interface UserServiceApi {

    /**
     * @Author fangzhenxun
     * @Description  根据用户名查询用户,
     *               注意：在使用feign 客户端的时候两边的参数一定要对应，
     * @Date  2019/11/5 17:41
     * @param username
     **/
    @ApiOperation(value = "根据唯一用户名查询用户（用户名可以是：手机号/邮箱/身份证号等唯一字符串）", response = ResponseResult.class)
    @ApiImplicitParams({@ApiImplicitParam(name = "username", value = "用户名", required = true, paramType = "path")})
    @GetMapping("/users/{username}")
    ResponseResult<UserDTO> getUserByUserName(@PathVariable("username") String username);

    @ApiOperation(value = "根据唯一oauth2 的客户端id获取客户端信息", response = ResponseResult.class)
    @ApiImplicitParams({@ApiImplicitParam(name = "clientId", value = "客户端信息id", required = true, paramType = "path")})
    @GetMapping("/client-details/{clientId}")
    ResponseResult<OauthClientDetailsDTO> loadClientByClientId(@PathVariable("clientId")String clientId);
}
