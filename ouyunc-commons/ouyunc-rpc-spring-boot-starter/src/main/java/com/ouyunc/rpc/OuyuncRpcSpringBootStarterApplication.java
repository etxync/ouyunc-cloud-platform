package com.ouyunc.rpc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.rpc"})
@Configuration
public class OuyuncRpcSpringBootStarterApplication {}
