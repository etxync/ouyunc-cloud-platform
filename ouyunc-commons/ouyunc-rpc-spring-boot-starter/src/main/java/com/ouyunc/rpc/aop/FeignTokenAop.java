package com.ouyunc.rpc.aop;

import com.ouyunc.common.constant.CommonConstant;
import com.ouyunc.common.utils.ThreadPoolUtil;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class FeignTokenAop {

    private static final Logger log = LoggerFactory.getLogger(FeignTokenAop.class);


    /**
     * 切点
     */
    @Pointcut(value = "@annotation(org.springframework.web.bind.annotation.RequestMapping) || @annotation(org.springframework.web.bind.annotation.GetMapping) || @annotation(org.springframework.web.bind.annotation.PostMapping) || @annotation(org.springframework.web.bind.annotation.DeleteMapping) || @annotation(org.springframework.web.bind.annotation.PutMapping)")
    public void pointcut(){};


    /**
     * @Author fangzhenxun
     * @Description  日志环绕通知, 注解@xxxMapping 继承 注解@RequestMapping
     * @Date 2020/12/21 15:52
     **/
    @Before(value = "pointcut()")
    public void logBefore() throws Throwable {
        log.info("正在处理feign 传递token的情况！");
        ServletRequestAttributes requestAttributes =(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String accessToken = request.getHeader(CommonConstant.ACCESS_TOKEN);
        ThreadPoolUtil.ttlThreadLocal.set(accessToken);
    }

   
}
