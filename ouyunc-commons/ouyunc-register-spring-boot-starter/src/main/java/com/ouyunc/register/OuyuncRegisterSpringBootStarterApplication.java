package com.ouyunc.register;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.register"})
@Configuration
public class OuyuncRegisterSpringBootStarterApplication {

}
