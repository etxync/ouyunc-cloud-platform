package com.ouyunc.db;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.db"})
@Configuration
public class OuyuncDbSpringBootStarterApplication {
}
