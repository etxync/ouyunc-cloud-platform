package com.ouyunc.db.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ouyunc.db.base.BaseEntity;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @description: 对数据库每条记录的创建时间和更新时间自动进行填充
 **/
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    /**
     * 插入时的填充策略
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        setFieldValByName(BaseEntity.Fields.createTime, LocalDateTime.now(), metaObject);
        setFieldValByName(BaseEntity.Fields.updateTime,  LocalDateTime.now(), metaObject);
        setFieldValByName(BaseEntity.Fields.deleted,  0, metaObject);
    }
   
    /**
     * 更新时的填充策略
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        setFieldValByName(BaseEntity.Fields.updateTime,  LocalDateTime.now(), metaObject);
    }
}