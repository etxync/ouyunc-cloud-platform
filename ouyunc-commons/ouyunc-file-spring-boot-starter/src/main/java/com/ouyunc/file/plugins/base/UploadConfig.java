package com.ouyunc.file.plugins.base;

import lombok.Builder;

/**
 * @Author fangzhenxun
 * @Description: 上传文件配置项
 * @Version V1.0
 **/
@Builder
public class UploadConfig {

    /**
     * 上传到的桶名:如 micro-report
     */
    private String bucketName;

    /**
     * 上传桶path例如：doc/picture/ 或doc/xxx.jpg, 标识唯一key
     */
    private String bucketPath;

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketPath() {
        return bucketPath;
    }

    public void setBucketPath(String bucketPath) {
        this.bucketPath = bucketPath;
    }
}
