package com.ouyunc.file.plugins;

import com.ouyunc.file.plugins.base.UploadConfig;
import com.ouyunc.file.plugins.base.FileReturn;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author fangzhenxun
 * @Description: 上传插件
 * @Version V1.0
 **/
public interface Uploader{






    /**
     * @Author fangzhenxun
     * @Description 上传文件
     * @param file
     * @param fileItem 配置项
     * @return java.lang.String
     */
    FileReturn upload(MultipartFile file, UploadConfig fileItem);


    /**
     * @Author fangzhenxun
     * @Description 赋值文件
     * @param srcFilePath
     * @param fileItem
     * @return java.lang.String
     */
    FileReturn copy(String srcFilePath, UploadConfig fileItem);

}
