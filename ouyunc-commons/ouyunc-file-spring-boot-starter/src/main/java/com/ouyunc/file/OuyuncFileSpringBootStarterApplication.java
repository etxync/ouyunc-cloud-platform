package com.ouyunc.file;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.file"})
@Configuration
public class OuyuncFileSpringBootStarterApplication {}
