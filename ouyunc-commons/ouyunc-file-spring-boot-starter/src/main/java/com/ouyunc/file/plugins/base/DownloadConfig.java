package com.ouyunc.file.plugins.base;


import lombok.Builder;

import java.util.Map;

/**
 * 定义response 的相关配置
  */
@Builder
public class DownloadConfig {

    /**
     * content-type, 默认空
     */
    private String contentType;

    /**
     * header, 响应头
     */
    private Map<String, String> headerMap;

    public DownloadConfig() {
    }

    public DownloadConfig(String contentType, Map<String, String> headerMap) {
        this.contentType = contentType;
        this.headerMap = headerMap;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Map<String, String> getHeaderMap() {
        return headerMap;
    }

    public void setHeaderMap(Map<String, String> headerMap) {
        this.headerMap = headerMap;
    }
}
