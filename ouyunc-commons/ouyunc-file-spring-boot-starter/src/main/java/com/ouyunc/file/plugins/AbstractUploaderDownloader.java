package com.ouyunc.file.plugins;

import com.ouyunc.common.constant.enums.FilePluginEnum;


public abstract class AbstractUploaderDownloader implements Uploader, Downloader{

    /**
     * 文件插件枚举
     */
    public abstract FilePluginEnum pluginEnum();
}
