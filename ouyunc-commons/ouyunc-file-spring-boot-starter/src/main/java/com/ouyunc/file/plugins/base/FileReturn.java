package com.ouyunc.file.plugins.base;

/**
 * @Author fangzhenxun
 * @Description: 文件上传/复制等返回的结果格式
 * @Version V1.0
 **/
public class FileReturn {
    /**
     * 文件原始名称; 如：偶云客技术文档.pdf
     */
    private String fileOriginName;

    /**
     * 文件当前名称; 如：2121212323_偶云客技术文档.pdf
     */
    private String fileName;

    /**
     * 文件后缀名称；如.zip, .doc
     */
    private String fileSuffix;

    /**
     * 文件访问url (完整路径：http(s)://);
     */
    private String fileUrl;

    /**
     * 文件访问url (完整路径：http(s)://);
     */
    private String filePath;

    /**
     * 文件大小，如: 12KB, 21MB, 1GB
     */
    private long fileSize;

    /**
     * 文件md5
     */
    private String fileMd5;


    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileOriginName() {
        return fileOriginName;
    }

    public void setFileOriginName(String fileOriginName) {
        this.fileOriginName = fileOriginName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileSuffix() {
        return fileSuffix;
    }

    public void setFileSuffix(String fileSuffix) {
        this.fileSuffix = fileSuffix;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileMd5() {
        return fileMd5;
    }

    public void setFileMd5(String fileMd5) {
        this.fileMd5 = fileMd5;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public FileReturn() {
    }

    public FileReturn(String fileOriginName, String fileName, String fileSuffix, String fileUrl, long fileSize) {
        this.fileOriginName = fileOriginName;
        this.fileName = fileName;
        this.fileSuffix = fileSuffix;
        this.fileUrl = fileUrl;
        this.fileSize = fileSize;
    }

    public FileReturn(String fileOriginName, String fileName, String fileSuffix, String fileUrl, String filePath, long fileSize, String fileMd5) {
        this.fileOriginName = fileOriginName;
        this.fileName = fileName;
        this.fileSuffix = fileSuffix;
        this.fileUrl = fileUrl;
        this.filePath = filePath;
        this.fileSize = fileSize;
        this.fileMd5 = fileMd5;
    }

    public FileReturn(String fileOriginName, String fileName, String fileSuffix, String fileUrl, long fileSize, String fileMd5) {
        this.fileOriginName = fileOriginName;
        this.fileName = fileName;
        this.fileSuffix = fileSuffix;
        this.fileUrl = fileUrl;
        this.fileSize = fileSize;
        this.fileMd5 = fileMd5;
    }
}
