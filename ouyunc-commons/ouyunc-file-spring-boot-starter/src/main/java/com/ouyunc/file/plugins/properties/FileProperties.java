package com.ouyunc.file.plugins.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author fangzhenxun
 * @Description: 上传/下载文件信息配置
 * @Version V1.0
 **/
@Data
@Component
@ConfigurationProperties(prefix = "file")
public class FileProperties {

    /**
     * minio 的相关配置
     */
    private Minio minio = new Minio();

    /**
     * 本地上传的相关配置,注意这里必须先new出来一个实例
     */
    private Local local = new Local();


    /**
     * 本地上传的相关配置,注意这里必须先new出来一个实例
     */
    private Cos cos = new Cos();

    @Data
    public class Cos {

        /**
         * cos 下载路径endpoint前缀
         */

        private String endpoint = "http://localhost";

        /**
         * cos 腾讯云账户secretId，secretKey
         */
        private String secretId = "secretId";

        /**
         * 腾讯云账户secretId，secretKey
         */
        private String secretKey = "secretKey";

        /**
         *  默认地区
         */
        private String region = "region";
    }


    @Data
    public class Local {

        /**
         * local 文件上传路径
         */
        private String uploadPath = "C:\\Users\\fzx\\Desktop\\";

        /**
         * local 下载路径endpoint前缀
         */
        private String endpoint = "http://localhost";

    }

    @Data
    public class Minio {
        /**
         * minio 文件上传端点
         */
        private String endpoint = "http://localhost";


        /**
         * minio 端口
         */
        private int port = 9000;


        /**
         * minio 默认地区
         */
        private String region = "shanghai";


        /**
         * minio key
         */
        private String accessKey = "minio";

        /**
         * minio secret
         */
        private String secretKey = "minio";



    }

}
