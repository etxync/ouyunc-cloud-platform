package com.ouyunc.file.plugins;

import com.ouyunc.file.plugins.base.DownloadConfig;

/**
 * @Author fangzhenxun
 * @Description: 下载插件
 * @Version V1.0
 **/

public interface Downloader{



    /**
     * @Author fangzhenxun
     * @Description 下载远端文件； http:// 开头的路径
     * @param fileUri
     * @param fileName
     * @return void
     */
    void download(String fileUri, String fileName, DownloadConfig config);

    /**
     * @Author fangzhenxun
     * @Description 下载远端文件； http:// 开头的路径
     * @param bytes  文件字节数组
     * @param fileName
     * @return void
     */
    void download(byte[] bytes, String fileName, DownloadConfig config);

}
