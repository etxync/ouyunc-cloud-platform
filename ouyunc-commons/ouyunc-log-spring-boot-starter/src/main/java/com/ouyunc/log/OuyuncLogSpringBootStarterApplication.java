package com.ouyunc.log;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.log"})
@Configuration
public class OuyuncLogSpringBootStarterApplication {}
