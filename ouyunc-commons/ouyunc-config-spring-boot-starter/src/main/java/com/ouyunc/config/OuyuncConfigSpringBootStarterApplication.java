package com.ouyunc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.config"})
@Configuration
public class OuyuncConfigSpringBootStarterApplication {



}
