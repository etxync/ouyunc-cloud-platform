package com.ouyunc.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

//https://docs.spring.io/spring-boot/docs/2.4.3/reference/html/production-ready-features.html#production-ready-endpoints
@ComponentScan(basePackages = {"com.ouyunc.monitor"})
@Configuration
public class OuyuncMonitorSpringBootStarterApplication {
}
