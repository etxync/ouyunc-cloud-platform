package com.ouyunc.common.utils;

import java.util.Map;

/**
 * @Author fangzhenxun
 * @Description: map工具类
 * @Version V1.0
 **/
public class MapUtil {
    /**
     * @Author fangzhenxun
     * 合并多个map
     * @Description  重载函数，默认覆盖key相同的值
     * @Date  2019/10/22 11:23
     * @Param [maps]
     * @return java.util.Map
     **/
    public static<K, V> Map mergerMaps(Map<K, V>... maps) {
        return mergerMaps(true, maps);
    }

    /**
     * @Author fangzhenxun
     * 合并多个map
     * @Description  根据cover如果key存在来决定是否覆盖key 对应的值，cover=true,覆盖，cover=false bu
     * @Date  2019/10/22 11:01
     * @Param [cover, maps]
     * @return java.util.Map
     **/
    public static<K, V> Map mergerMaps(boolean cover, Map<K, V>... maps) {
        // 获取传入map的类型
        Class clazz = maps[0].getClass();
        Map<K, V> map = null;
        try {
            map = (Map) clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Map<K, V> myMap : maps) {
            for (Map.Entry<K, V> entry : myMap.entrySet()) {
                map.merge(entry.getKey(), entry.getValue(), (oldV, newV) -> cover ? newV : oldV);
            }
        }
        return map;
    }
}
