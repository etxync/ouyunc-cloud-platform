package com.ouyunc.common.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author fangzhenxun
 * @date 2019/11/5 17:07
 * @description 所有开放权限
 */
public class PermitAllUrl {

    /**
     * 监控中心和swagger需要访问的url,注意路径在最前边一定要加上 /
     */
    private static final String[] ANT_PATH_ENDPOINTS = {"/js/**", "/css/**", "/images/**", "/actuator/health", "/actuator/env", "/actuator/metrics/**", "/actuator/trace", "/actuator/dump",
            "/actuator/jolokia", "/actuator/info", "/actuator/logfile", "/actuator/refresh", "/actuator/flyway", "/actuator/liquibase",
            "/actuator/heapdump", "/actuator/loggers", "/actuator/auditevents", "/actuator/env/PID", "/actuator/jolokia/**",
            "/**/v2/api-docs/**","/**/v3/api-docs/**", "/api-docs/**", "/swagger/**", "/swagger-resources/**", "/webjars/**", "/swagger-ui.html", "/doc.html", "/api-docs-ext/**"};

    private static final String[] PATH_PATTERN_ENDPOINTS = {"/js/**", "/css/**", "/images/**", "/actuator/health", "/actuator/env", "/actuator/metrics/**", "/actuator/trace", "/actuator/dump",
            "/actuator/jolokia", "/actuator/info", "/actuator/logfile", "/actuator/refresh", "/actuator/flyway", "/actuator/liquibase",
            "/actuator/heapdump", "/actuator/loggers", "/actuator/auditevents", "/actuator/env/PID", "/actuator/jolokia/**",
            "/*/v2/api-docs/**","/*/v3/api-docs/**", "/api-docs/**", "/swagger/**", "/swagger-resources/**", "/webjars/**", "/swagger-ui.html", "/doc.html", "/api-docs-ext/**"};

    /**
     * @Author fangzhenxun
     * @Description  需要放开权限的url, 自定义的url和监控中心需要访问的url集合
     * @Date  2019/11/5 17:08
     * @Param antPath MVC 路径匹配策略， true-> ant_path_matcher, false->path_pattern_parser
     * @Param [urls] 自定义的需要开饭url
     * @return java.lang.String[]
     **/
    public static String[] permitAllUrl(boolean antPath, String... urls) {
        String[] ENDPOINTS = ANT_PATH_ENDPOINTS;
        if (!antPath) {
            ENDPOINTS = PATH_PATTERN_ENDPOINTS;
        }
        if (urls == null || urls.length == 0) {
            return ENDPOINTS;
        }
        Set<String> set = new LinkedHashSet<>();
        Collections.addAll(set, ENDPOINTS);
        Collections.addAll(set, urls);
        return set.toArray(new String[set.size()]);
    }
    /**
     * @Author fangzhenxun
     * @Description  需要放开权限的url, 自定义的url和监控中心需要访问的url集合
     * 默认true antPath 匹配方式的路径
     * @Date  2019/11/5 17:08
     * @Param [urls] 自定义的需要开饭url
     * @return java.lang.String[]
     **/
    public static String[] permitAllUrl(String... urls) {
        return permitAllUrl(true, urls);
    }
    /**
     * @Author fangzhenxun
     * @Description  返回set集合
     * @Date 2020/4/13 19:24
     * @param antPath 路径匹配规则 antpath
     * @param urls
     * @return java.util.Set<java.lang.String>
     **/
    public static Set<String> permitAllUrlSet(boolean antPath,String... urls) {
        String[] ENDPOINTS = ANT_PATH_ENDPOINTS;
        if (!antPath) {
            ENDPOINTS = PATH_PATTERN_ENDPOINTS;
        }
        if (urls == null || urls.length == 0) {
            return Arrays.stream(ENDPOINTS).collect(Collectors.toSet());
        }
        Set<String> set = new LinkedHashSet<>();
        Collections.addAll(set, ENDPOINTS);
        Collections.addAll(set, urls);
        return set;
    }

    /**
     * @Author fangzhenxun
     * @Description  返回set集合
     * @Date 2020/4/13 19:24
     * @param urls
     * @return java.util.Set<java.lang.String>
     **/
    public static Set<String> permitAllUrlSet(String... urls) {
        return permitAllUrlSet(true, urls);
    }
}
