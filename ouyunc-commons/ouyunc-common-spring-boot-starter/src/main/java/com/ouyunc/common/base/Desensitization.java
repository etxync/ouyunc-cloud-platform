package com.ouyunc.common.base;

/**
 * @Author fangzhenxun
 * @Description: 脱敏接口,主要针对用户实体，返回到前端不想被用户看到敏感的信息
 * @Version V1.0
 **/
public interface Desensitization<T> {

    /**
     * @Author fangzhenxun
     * @Description 脱敏
     * @param
     * @return T
     */
    T desensitization();
}
