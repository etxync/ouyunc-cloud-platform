package com.ouyunc.common.utils;

/**
 * @Author fangzhenxun
 * @Description: 字符串工具类
 * @Version V1.0
 **/
public class StringUtil {

    /**
     * @Author fangzhenxun
     * @Description 驼峰转下划线
     * @param camel
     * @return java.lang.String
     */
    public static String camel2underline(String camel){
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < camel.length(); ++i) {
            char ch = camel.charAt(i);
            if (ch >= 'A' && ch <= 'Z') {
                char ch_ucase = (char) (ch + 32);
                if (i > 0) {
                    buf.append('_');
                }
                buf.append(ch_ucase);
            } else {
                buf.append(ch);
            }
        }
        return buf.toString();
    }


    /**
     * @Author fangzhenxun
     * @Description 下划线转驼峰
     * @param underline
     * @param firstLower  首字母是否大写 true-大写， false-小写
     * @return java.lang.String
     */
    public static String underline2camel(String underline, boolean firstLower){
        if (underline == null) {
            return null;
        }
        String[] split = underline.split("_");
        StringBuffer sb = new StringBuffer(underline.length());
        for (int i = 0; i< split.length; i++) {
            char[] chars = split[i].toCharArray();
            if(chars[0] >= 'A' && chars[0] <='Z'){

            }else if(chars[0] >='a' && chars[0] <= 'z'){
                // 如果是首个单词，则第一个为小写
                if (firstLower && i == 0) {
                    // do nothing
                }else {
                    chars[0] -=32;
                }
            }

            sb.append(chars);
        }
        return sb.toString();
    }


    /**
     * @Author fangzhenxun
     * @Description 下划线转驼峰, 默认首单词首字母小写
     * @param underline
     * @return java.lang.String
     */
    public static String underline2camel(String underline){
        return underline2camel(underline, true);
    }

}
