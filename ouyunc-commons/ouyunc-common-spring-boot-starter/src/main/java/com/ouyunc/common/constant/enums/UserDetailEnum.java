package com.ouyunc.common.constant.enums;

/**
 * @Author fangzhenxun
 * @Description 用户详情枚举类型
 * @Date 2020/7/3 14:42
 **/
public enum UserDetailEnum {

    USERNAME("username", "对应userDetail中的字段属性username"),
    PASSWORD("password", "对应userDetail中的字段属性password"),
    AUTHORITIES("authorities", "对应userDetail中的字段属性authorities");


    /**
     * 对应userDetail中的字段属性名称值
     **/
    private String fieldName;

    /**
     * 对应userDetail中的字段属性名称描述
     **/
    private String description;

    UserDetailEnum(String fieldName, String description) {
        this.fieldName = fieldName;
        this.description = description;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
