package com.ouyunc.common.constant;

/**
 * @Author fangzhenxun
 * @Description redis 的key值常量prefix 前缀在这里定义
 * @Date 2020/4/28 16:20
 **/
public class RedisKeyConstant {

    /**
     * 用户手机号验证码
     **/
    public static final String USER_PHONE_VERIFICATION_CODE = "user:phone:verification-code:";

    /**
     * 接口所对应的哪些角色可以访问
     **/
    public static final String MANAGER_INTERFACE_ROLE = "manager:interface:role:";

    /**
     * 默认oauth2生成的token在redis中存储的key前缀
     **/
    public static final String DEFAULT_OAUTH2_TOKEN_KEY_PREFIX = "oauth2-token:";



}
