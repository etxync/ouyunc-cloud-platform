package com.ouyunc.common;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.common"})
@Configuration
public class OuyuncCommonSpringBootStarterApplication {}
