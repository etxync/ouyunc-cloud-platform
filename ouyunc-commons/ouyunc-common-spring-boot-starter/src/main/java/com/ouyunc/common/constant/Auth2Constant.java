package com.ouyunc.common.constant;

/**
 * @Author fangzhenxun
 * @Description oauth2 相关常量
 * @Date 2020/4/13 9:13
 **/
public class Auth2Constant {
    /**
     * utf-8编码
     */
    public static final String UTF_8 = "UTF-8";

    //================================================== oauth2 相关常量 ==================================================



    /**
     * 客户端id
     */
    public static final String CLIENT_ID = "client_id";

    /**
     * 客户端密码
     */
    public static final String CLIENT_SECRET = "client_secret";

    /**
     * 客户端当前的状态，可以指定任意值，认证服务器会原封不动的返回这个值
     */
    public static final String STATE = "state";

    /**
     * 申请的权限范围
     */
    public static final String SCOPE = "scope";

    /**
     * 获得的授权吗
     */
    public static final String CODE = "code";

    /**
     * 重定向URI
     */
    public static final String REDIRECT_URI = "redirect_uri";

    /**
     * 授权类型，必选，此处固定值“code”
     */
    public static final String RESPONSE_TYPE = "response_type";

    /**
     * 用户批准
     */
    public static final String USER_OAUTH_APPROVAL = "user_oauth_approval";

    /**
     * 作用域前缀
     */
    public static final String SCOPE_PREFIX = "scope.";

    /**
     * 表示使用的授权模式
     */
    public static final String GRANT_TYPE = "grant_type";

    /**
     * 表示访问令牌
     */
    public static final String ACCESS_TOKEN = "access_token";


    /**
     * 表示刷新令牌
     */
    public static final String REFRESH_TOKEN = "refresh_token";


    /**
     * 表示令牌类型，该值大小写不敏感
     */
    public static final String TOKEN_TYPE = "token_type";

    /**
     * 表示过期时间，单位为秒
     */
    public static final String EXPIRES_IN = "expires_in";


    /**
     * 用户名
     */
    public static final String USERNAME = "username";

    /**
     * 密码
     */
    public static final String PASSWORD = "password";

    /**
     * 授权码
     */
    public static final String AUTHORIZATION_CODE = "authorization_code";

    /**
     * loadUserByUsername 的扩展字段
     */
    public static final String EXTEND_MAP = "extendMap";

    /**
     * USER_TYPE 在扩展字段中的用户类型
     */
    public static final String USER_TYPE = "usertype";



    /**
     * 授权码登录错误提示
     */
    public static final String LOGIN_ERROR_MSG = "login_error_msg";

    //===============================================  oauth2 自定义授权码登录相关的常量 ===========================================


    /**
     * 自定义登录页面表单中对应的action
     **/
    public static final String LOGIN_PROCESS_URL_KEY = "loginProcessUrl";

    /**
     * 自定义登录页面表单中对应的action
     **/
    public static final String LOGIN_PROCESS_URL = "/authentication/form";

    /**
     * oauth2 框架认证的url path
     **/
    public static final String OAUTH_AUTHORIZE_URL = "/oauth/authorize";


    /**
     * 自定义oauth2 框架的校验权限
     **/
    public static final String OAUTH2_CONFIRM_ACCESS = "/oauth2/confirm_access-page";

    /**
     * 自定义oauth2 框架的授权错误页面
     **/
    public static final String OAUTH2_ERROR = "/oauth2/error-page";

    /**
     * 自定义oauth2 框架的登录路径
     **/
    public static final String OAUTH2_LOGIN = "/oauth2/login-page";

    /**
     * 在授权码登陆的时候，表单的登录类型key
     **/
    public static final String FORM_LOGIN_TYPE = "loginType";

    /**
     * 在授权码登陆的时候，表单的登录类型username-password
     **/
    public static final String FORM_LOGIN_TYPE_USERNAME_PASSWORD = "USERNAME_PASSWORD";

    /**
     * 在授权码登陆的时候，表单的登录类型username-password
     **/
    public static final String FORM_LOGIN_TYPE_VERIFICATION_CODE = "VERIFICATION_CODE";


    /**
     * 在请求头中存储的用户信息
     **/
    public static final String HEADER_PRINCIPLE = "PRINCIPLE";

    /**
     * 请求头token 前面的前缀
     **/
    public static final String BEARER_TYPE = "Bearer";



    /**
     * 认证与授权的头部
     **/
    public static final String AUTHORIZATION = "Authorization";

    /**
     * 登录用户的详情字段key
     **/
    public static final String USER_DETAIL = "userDetail";

    /**
     * user_name ,jwt 中存储的key
     **/
    public static final String USER_NAME = "user_name";




}
