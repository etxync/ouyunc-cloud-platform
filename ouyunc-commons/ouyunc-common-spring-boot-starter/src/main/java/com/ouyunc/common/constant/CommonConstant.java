package com.ouyunc.common.constant;

/**
 * @Author fangzhenxun
 * @Description 公共业务常量
 * @Date 2020/12/15 13:31
 **/
public class CommonConstant {

    /**
     * utf-8 编码
     **/
    public static final String UTF_8 = "UTF-8";

    /**
     * 服务应用名称
     **/
    public static final String APPLICATION_NAME_KEY = "spring.application.name";

    /**
     * 当前请求所有 headers
     */
    public static final String CURRENT_REQUEST_HEADERS = "current_request_headers";

    /**
     * 当前请求所有 params 请求参数
     */
    public static final String CURRENT_REQUEST_PARAMS = "current_request_params";
    /**
     * 日志跟踪id
     */
    public static final String LOG_TRACE_ID = "trace_id";

    /**
     * 日志跟踪spanid
     */
    public static final String LOG_SPAN_ID = "span_id";
    /**
     * 日志跟踪pspanId  父spanId
     */
    public static final String LOG_PARENT_SPAN_ID = "p_span_id";

    /**
     * 日志请求接口所在的服务id
     */
    public static final String LOG_SERVER_IP = "server_ip";

    /**
     * 日志请求接口所在的服务id
     */
    public static final String LOG_SERVER_NAME = "server_name";

    /**
     * 日志请求接口的url
     */
    public static final String LOG_REQUEST_URI = "request_uri";

    /**
     * 日志请求方的客户端ip
     */
    public static final String LOG_CLIENT_IP = "client_ip";

    /**
     * access token
     */
    public static final String ACCESS_TOKEN = "access_token";

    /**
     * 请求头
     */
    public static final String AUTHORIZATION = "Authorization";

    /**
     * 等待好友/群验证
     */
    public static final Integer FRIEND_OR_GROUP_WAIT_FOR_VERIFICATION_HANDLER_STATUS = 0;


    /**
     * 好友/群同意
     */
    public static final Integer FRIEND_OR_GROUP_AGREE_HANDLER_STATUS = 1;

    /**
     * 好友/群拒绝
     */
    public static final Integer FRIEND_OR_GROUP_REFUSE_HANDLER_STATUS = 2;

    /**
     * 群邀请同意,被邀请人同意，等待群管理员处理
     */
    public static final Integer GROUP_INVITE_AGREE_HANDLER_STATUS = 3;

    /**
     * 修改群昵称
     */
    public static final Integer UPDATE_GROUP_NICKNAME = 1;

    /**
     * 修改群用户昵称
     */
    public static final Integer UPDATE_GROUP_USER_NICKNAME = 2;

    /**
     * 临时聊天联系人列表
     */
    public static final String TEMP_CHAT_CONTACT = "temp:chat-contact:";
}
