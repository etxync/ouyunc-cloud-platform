package com.ouyunc.common.utils;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.alibaba.ttl.threadpool.TtlExecutors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 公共线程池创建类
 */
public class ThreadPoolUtil {
    private static final Logger log = LoggerFactory.getLogger(ThreadPoolUtil.class);

    public static final ThreadLocal<Object> ttlThreadLocal = new TransmittableThreadLocal<>();


    /**
     * 根获取的是cpu核心线程数也就是计算资源。
     */
    private static final int AVAILABLE_CUPS = Runtime.getRuntime().availableProcessors();
    /**
     * 核心线程数 = CPU核心数 + 1
     */
    private static final int CORE_POOL_SIZE = AVAILABLE_CUPS + 1;
    /**
     * 线程池最大线程数 = CPU核心数 * 2 + 1
     */
    private static final int MAXIMUM_POOL_SIZE = AVAILABLE_CUPS * 2 + 1;
    /**
     * 非核心线程闲置时间 = 超时1s
     */
    private static final long KEEP_ALIVE = 1L;
    /**
     * 队列最大容量,默认integer 最大值
     */
    private static final int QUEUE_CAPACITY = Integer.MAX_VALUE;

    private static volatile ThreadPoolExecutor threadPoolExecutor;


    private ThreadPoolUtil() {
    }

    /**
     * @param corePoolSize    线程池核心线程大小
     * @param maximumPoolSize 线程池最大线程数
     * @param keepAliveTime   空闲线程存活时间
     * @param unit            空闲线程存活时间单位
     * @param workQueue       工作队列
     * @param threadFactory   工厂模式
     * @param rejectedHandler 拒绝策略
     * @return
     */
    public static ExecutorService threadPool(int corePoolSize,
                                      int maximumPoolSize,
                                      long keepAliveTime,
                                      TimeUnit unit,
                                      BlockingQueue<Runnable> workQueue,
                                      ThreadFactory threadFactory,
                                      RejectedExecutionHandler rejectedHandler) {
        return new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, rejectedHandler);
    }


    /**
     * @Author fangzhenxun
     * @Description 获取线程池
     * @param
     * @return java.util.concurrent.ThreadPoolExecutor
     */
    public static ExecutorService defaultThreadPool() {
        if (threadPoolExecutor == null) {
            synchronized (ThreadPoolUtil.class) {
                if (threadPoolExecutor == null) {
                    log.info("开始创建线程池：心线程数=>{}, 最大线程数=>{},线程最大空闲时间=>{}", CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE);
                    return new ThreadPoolExecutor(CORE_POOL_SIZE,
                            MAXIMUM_POOL_SIZE,
                            KEEP_ALIVE,
                            TimeUnit.SECONDS, new LinkedBlockingQueue(QUEUE_CAPACITY),
                            new ThreadFactory() {
                                private final AtomicInteger threadNumber = new AtomicInteger(1);
                                private final AtomicInteger poolNumber = new AtomicInteger(1);
                                /**
                                 * @Author fangzhenxun
                                 * @Description 创建线程
                                 */
                                public Thread newThread(Runnable runnable) {
                                    SecurityManager s = System.getSecurityManager();
                                    ThreadGroup group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
                                    Thread t = new Thread(group, runnable,"ouyunc-pool-" + poolNumber.getAndIncrement() + "-thread-" + threadNumber.getAndIncrement(),0);
                                    if (t.isDaemon()) {
                                        t.setDaemon(false);
                                    }
                                    if (t.getPriority() != Thread.NORM_PRIORITY) {
                                        t.setPriority(Thread.NORM_PRIORITY);
                                    }
                                    return t;
                                }
                            },
                            new ThreadPoolExecutor.DiscardPolicy());
                }
            }
        }
        return threadPoolExecutor;
    }

    /** 使用场景： 父子线程共享数据如 httpServletRequest, 会话信息等
     * 注意下面这种方式，满足的条件是主线程的等在子线程完成后在结束，才可以保证子线程拿到request，所以不考虑使用
     * RequestContextHolder.setRequestAttributes(servletRequestAttributes,true);
     * @param corePoolSize    线程池核心线程大小
     * @param maximumPoolSize 线程池最大线程数
     * @param keepAliveTime   空闲线程存活时间
     * @param unit            空闲线程存活时间单位
     * @param workQueue       工作队列
     * @param threadFactory   工厂模式
     * @param rejectedHandler 拒绝策略
     * @return
     */
    public static ExecutorService ttlThreadPool(int corePoolSize,
                                         int maximumPoolSize,
                                         long keepAliveTime,
                                         TimeUnit unit,
                                         BlockingQueue<Runnable> workQueue,
                                         ThreadFactory threadFactory,
                                         RejectedExecutionHandler rejectedHandler) {
        return TtlExecutors.getTtlExecutorService(threadPool(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, rejectedHandler));
    }

    /**
     * 获取默认ttl 本地线程池
     * @return
     */
    public static ExecutorService defaultTtlThreadPool() {
        return TtlExecutors.getTtlExecutorService(defaultThreadPool());
    }

}
