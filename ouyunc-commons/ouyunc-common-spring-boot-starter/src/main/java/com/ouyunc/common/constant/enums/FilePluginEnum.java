package com.ouyunc.common.constant.enums;

/**
 * @Author fangzhenxun
 * @Description: 文件插件枚举
 * @Version V1.0
 **/
public enum FilePluginEnum {


    LOCAL("本地"),
    COS("腾讯云"),
    OBS("华为云"),
    OSS("阿里云"),
    QI_NIU("七牛云"),
    FAST_DFS("fastdfs"),
    MIN_IO("minio");

    private String pluginName;

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }

    FilePluginEnum(String pluginName) {
        this.pluginName = pluginName;
    }
}
