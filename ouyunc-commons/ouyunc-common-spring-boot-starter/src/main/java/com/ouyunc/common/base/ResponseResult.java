package com.ouyunc.common.base;



import com.ouyunc.common.constant.enums.ResponseCodeEnum;
import lombok.experimental.FieldNameConstants;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static com.ouyunc.common.constant.enums.ResponseCodeEnum.*;


/**
 * @Author fangzhenxun
 * @Description 统一返回体
 * @Date 2020/4/9 10:28
 **/
@FieldNameConstants
public class ResponseResult<T> implements Serializable {

    private static final long serialVersionUID = 2047703336323178317L;
    /**
     * 返回码
     **/
    private int code;

    /**
     * 错误信息
     **/
    private String message;

    /**
     * 返回体信息
     **/
    private T data;

    /**
     * 是否成功，成功-程序按照可预知的方向执行，失败-程序不可预知错误
     **/
    private boolean success;

    /**
     * 当前时间戳毫秒
     **/
    private long timestamp;

    public ResponseResult() {
    }

    public ResponseResult(int code, String message, T data, boolean success) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.success = success;
        this.timestamp = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public static ResponseResult common(int code, String message, Object data, boolean success) {
        return new ResponseResult(code, message, data, success);
    }


    public static ResponseResult success() {
        return new ResponseResult(OK.code(), OK.getDescription(), null, true);
    }

    public static ResponseResult success(Object data) {
        return new ResponseResult(OK.code(), OK.getDescription(), data, true);
    }

    public static ResponseResult success(ResponseCodeEnum codeEnum, Object data) {
        return new ResponseResult(codeEnum.code(), codeEnum.getDescription(), data, true);
    }

    public static ResponseResult fail() {
        return new ResponseResult(BAD_REQUEST.code(), BAD_REQUEST.getDescription(), null, false);
    }

    public static ResponseResult fail(String message) {
        return new ResponseResult(BAD_REQUEST.code(), message, null, false);
    }

    public static ResponseResult fail(ResponseCodeEnum codeEnum) {
        return new ResponseResult(codeEnum.code(), codeEnum.getDescription(), null, false);
    }

    public static ResponseResult fail(ResponseCodeEnum codeEnum, String message) {
        return new ResponseResult(codeEnum.code(), message, null, false);
    }

    public static ResponseResult error() {
        return new ResponseResult(INTERNAL_SERVER_ERROR.code(), INTERNAL_SERVER_ERROR.getDescription(), null, false);
    }

    public static ResponseResult error(String message) {
        return new ResponseResult(INTERNAL_SERVER_ERROR.code(), message, null, false);
    }

    public static ResponseResult error(ResponseCodeEnum codeEnum) {
        return new ResponseResult(codeEnum.code(), codeEnum.getDescription(), null, false);
    }

    public static ResponseResult error(ResponseCodeEnum codeEnum, String message) {
        return new ResponseResult(codeEnum.code(), message, null, false);
    }
}
