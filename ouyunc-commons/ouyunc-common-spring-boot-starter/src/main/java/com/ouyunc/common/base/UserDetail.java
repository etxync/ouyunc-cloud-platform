package com.ouyunc.common.base;


import com.ouyunc.common.constant.enums.UserDetailEnum;

import java.lang.annotation.*;

/**
 * @Author fangzhenxun
 * @Description  实体类字段所对应spring security中 userDetail的哪些字段,
 * 注意，在定义实体类中用户名和密码是字符串类型，权限是Set<String> 类型
 * @Date 2020/7/3 14:41
 **/
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface UserDetail {
    /**
     * 字段属性名称枚举 ,
     **/
    UserDetailEnum fieldName();
}
