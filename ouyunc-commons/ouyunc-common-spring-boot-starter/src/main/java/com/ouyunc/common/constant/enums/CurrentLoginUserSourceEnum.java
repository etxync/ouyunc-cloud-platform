package com.ouyunc.common.constant.enums;

/**
 * @Author fangzhenxun
 * @Description: 当前登录用户来源枚举，可自行扩展设计
 * @Version V1.0
 **/
public enum CurrentLoginUserSourceEnum {
    // 从redis 中读取
    REDIS,
    // 从请求头中读取
    HEADER,
    // 从jwt 解析
    JWT,
    // 先从header中读取，如果读取不到在从redis中读取
    HEADER_REDIS,
    // 先从redis中读取，如果读取不到在从header中读取
    REDIS_HEADER,
}
