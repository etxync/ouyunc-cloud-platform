package com.ouyunc.common.utils;

import com.alibaba.fastjson2.JSON;
import org.apache.commons.lang3.StringUtils;
import org.yaml.snakeyaml.Yaml;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Deacription 支持读取${}占位符中的内容
 * @Author levi
 * @Date 2022/06/16 9:43
 * @Version 1.0
 **/
public class YmlUtil {

    // ${} 占位符 正则表达式
    private static Pattern p1 = Pattern.compile("\\$\\{.*?\\}");

    private YmlUtil(){
        throw new AssertionError();
    }

    /**
     * key:文件索引名
     * value：配置文件内容
     */
    private static Map<String , LinkedHashMap> ymls = new HashMap<>();
    /**
     * String:当前线程需要查询的文件名
     */
    private static ThreadLocal<String> nowFileName = new InheritableThreadLocal<>();

    private static ThreadLocal<String> profileLocal = new InheritableThreadLocal<>();

    /**
     * 主动设置，初始化当前线程的环境
     * @param profile
     */
    public static void setProfile(String profile) {
        profileLocal.set(profile);
    }

    /**
     * 加载配置文件
     * @param fileName
     */
    private static void loadYml(String fileName){
        nowFileName.set(fileName);
        if (!ymls.containsKey(fileName)){
            ymls.put(fileName , new Yaml().loadAs(YmlUtil.class.getResourceAsStream("/" + fileName), LinkedHashMap.class));
        }
    }

    /**
     * 读取yml文件中的某个value。
     * 支持解析 yml文件中的 ${} 占位符
     * @param key
     * @return Object
     */
    private static Object getValue(String key){
        String[] keys = key.split("[.]");
        Map ymlInfo = (Map) ymls.get(nowFileName.get()).clone();
        for (int i = 0; i < keys.length; i++) {
            Object value = ymlInfo.get(keys[i]);
            if (value == null){
                return null;
            }else if (i < keys.length - 1){
                ymlInfo = (Map) value;
            }else {
                String g;
                String keyChild;
                String v1 = value+"";
                for(Matcher m = p1.matcher(v1); m.find(); value = v1.replace(g, (String)getValue(keyChild))) {
                    g = m.group();
                    keyChild = g.replaceAll("\\$\\{", "").replaceAll("\\}", "");
                }
                return value;
            }
        }
        return "";
    }

    /**
     * 读取yml文件中的某个value
     * @param fileName  yml名称
     * @param key
     * @return Object
     */
    public static Object getValue(String fileName , String key){
        if (StringUtils.isBlank(fileName)) {
            fileName = "application.yml";
        }
        loadYml(fileName);
        return getValue(key);
    }

    /**
     * 读取yml文件中的某个value，返回String
     * @param fileName
     * @param key
     * @return String
     */
    public static<T> T getValue(String fileName , String key, Class<T> tClass){
        Object value = getValue(fileName, key);
        return JSON.parseObject(JSON.toJSONString(value), tClass);
    }

    /**
     *  获取 application-test/prod.yml 的配置
     * @param key
     * @return
     */
    public static<T> T getActiveProfileValue(String key, Class<T> tClass){
        String fileName = "application.yml";
        String activeProfiles = getActiveProfiles();
        if (StringUtils.isNotBlank(activeProfiles)) {
            fileName = "application-" + activeProfiles + ".yml";
        }
        return getValue(fileName, key, tClass);
    }

    /**
     * 框架私有方法，非通用。
     * 获取 spring.profiles.active的值: test/prod 测试环境/生成环境
     * @return
     */
    public static String getActiveProfiles(){
        if (profileLocal.get() == null) {
            String value = (String) getValue("application.yml", "spring.profiles.active");
            setProfile(value);
        }
        return profileLocal.get();
    }

}
