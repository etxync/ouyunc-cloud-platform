package com.ouyunc.cache.redisson.strategy.impl;
import com.ouyunc.cache.redisson.enums.RedissonEnum;
import com.ouyunc.cache.redisson.properties.SentinelRedissonProperties;
import com.ouyunc.cache.redisson.properties.StandaloneRedissonProperties;
import com.ouyunc.cache.redisson.strategy.RedissonStrategy;
import com.ouyunc.common.context.SpringContextHolder;
import com.ouyunc.common.utils.YmlUtil;
import org.redisson.config.Config;
import org.redisson.config.ReadMode;
import org.redisson.config.SentinelServersConfig;
import org.springframework.util.StringUtils;

/**
 * @author fangzhenxun
 * @date 2020/1/13 14:39
 * @description redisson哨兵模式的策略配置
 */
public class SentinelRedissonStrategy implements RedissonStrategy {

    /**
     * redisson哨兵配置信息
     **/
    private static final SentinelRedissonProperties sentinelRedissonProperties;

    static {
        sentinelRedissonProperties = YmlUtil.getActiveProfileValue("cache.redis.sentinel.redisson", SentinelRedissonProperties.class);
    }

    /**
     * @param
     * @return com.xyt.cache.config.redis.lettuce.enums.RedisEnum
     * @author fangzhenxun
     * @description redisson 标识该策略是哨兵模式的策略
     * @date 2020/1/13 14:43
     **/
    @Override
    public RedissonEnum getType() {
        return RedissonEnum.SENTINEL;
    }

    /**
     * @param
     * @return org.redisson.config.Config
     * @author fangzhenxun
     * @description redisson哨兵模式的构建
     * @date 2020/1/13 14:46
     **/
    @Override
    public Config buildConfig(int database) {
        Config config = new Config();
        SentinelServersConfig sentinelServersConfig = config.useSentinelServers()
                .setDatabase(sentinelRedissonProperties.getDatabase())
                .setMasterName(sentinelRedissonProperties.getMaster())
                .addSentinelAddress(sentinelRedissonProperties.getNodes())
                //设置只读节点
                .setReadMode(ReadMode.SLAVE)
                .setConnectTimeout(sentinelRedissonProperties.getConnTimeout())
                .setTimeout(sentinelRedissonProperties.getTimeout())
                .setIdleConnectionTimeout(sentinelRedissonProperties.getSoTimeout())
                .setMasterConnectionMinimumIdleSize(sentinelRedissonProperties.getPool().getMinIdle())
                .setMasterConnectionPoolSize(sentinelRedissonProperties.getPollSize())
                .setSlaveConnectionPoolSize(sentinelRedissonProperties.getPollSize());
        //如果密码不为空则设置密码
        if (!StringUtils.hasLength(sentinelRedissonProperties.getPassword())) {
            sentinelServersConfig.setPassword(sentinelRedissonProperties.getPassword());
        }
        if (0 <= database && database <= 16) {
            sentinelServersConfig.setDatabase(database);
        }
        return config;
    }
}
