package com.ouyunc.cache.redis.properties;

import io.lettuce.core.internal.LettuceSets;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author fangzhenxun
 * @date 2020/1/9 13:09
 * @description redis 集群模式的配置 cache.redis.cluster.lettuce
 */
@Data
public class ClusterRedisProperties {

    /**
     * redis 主机节点地址 + 端口号以英文逗号隔开
     **/
    private String nodes = "127.0.0.1:6379";

    /**
     * redis 集群模式下，集群最大转发的数量
     **/
    private Integer maxRedirects = 3;


    /**
     * redis 密码
     **/
    private String password;

    /**
     * redis 连接超时时间
     **/
    private Long timeout = 1000L;

    /**
     * redis lettuce 连接池的配置
     **/
    private RedisProperties.Pool pool = new RedisProperties.Pool();

    /**
     * @author fangzhenxun
     * @description  集群节点的节点配置
     * @date  2020/1/9 17:50
     * @param
     * @return java.util.Collection<java.lang.String>
     **/
    public Collection<String> getNodes() {
        Collection<String> clusterNodes = LettuceSets.newHashSet();
        if (StringUtils.isNotBlank(nodes)) {
            clusterNodes = Stream.of(nodes.split(",")).map(node ->node.trim()).collect(Collectors.toSet());
        }
        return clusterNodes;
    }
}
