package com.ouyunc.cache.redis.properties;

import io.lettuce.core.internal.LettuceSets;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author fangzhenxun
 * @date 2020/1/9 13:09
 * @description redis 哨兵模式的配置 cache.redis.sentinel.lettuce
 */
@Data
public class SentinelRedisProperties {

    /**
     * redis 主节点的名称（在哨兵配置文件中配置的）
     **/
    private String master = "mymaster";

    /**
     * redis 主机节点地址 + 端口号以英文逗号隔开
     **/
    private String nodes = "127.0.0.1:26379";

    /**
     * redis 连接数据库配置,默认0
     **/
    private Integer database = 0;

    /**
     * redis 密码
     **/
    private String password;

    /**
     * redis 连接超时时间
     **/
    private Long timeout = 1000L;

    /**
     * redis lettuce 连接池的配置
     **/
    private RedisProperties.Pool pool = new RedisProperties.Pool();

    /**
     * @author fangzhenxun
     * @description  将nodes 封装成set集合,nodes 的格式是用英文逗号隔开的host:port，
     * @date  2020/1/9 15:15
     * @param
     * @return java.lang.String
     **/
    public Set<String> getNodes() {
        Set<String> sentinelHostAndPorts = LettuceSets.newHashSet();
        if (StringUtils.isNotBlank(nodes)) {
            sentinelHostAndPorts = Stream.of(nodes.split(",")).map(node ->node.trim()).collect(Collectors.toSet());
        }
        return sentinelHostAndPorts;
    }
}
