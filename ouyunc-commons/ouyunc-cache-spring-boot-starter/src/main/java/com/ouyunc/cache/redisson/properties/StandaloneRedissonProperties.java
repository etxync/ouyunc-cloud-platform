package com.ouyunc.cache.redisson.properties;

import lombok.Data;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author fangzhenxun
 * @date 2020/1/13 13:33
 * @description redisson 的单例配置信息实体类 cache.redis.standalon.redisson
 */
@Data
public class StandaloneRedissonProperties {

    /**
     * redis 连接数据库配置,默认0
     **/
    private Integer database = 0;

    /**
     * redis 主机地址
     **/
    private String host = "127.0.0.2";

    /**
     * redis 主机端口号
     **/
    private Integer port = 6379;

    /**
     * redis 主机密码
     **/
    private String password;

    /**
     * redis 响应超时时间
     **/
    private Integer timeout = 1000;

    /**
     * redis 连接超时时间
     **/
    private Integer connTimeout = 10000;

    /**
     * 如果池连接未用于<code>超时</code>时间
     * 当前连接数大于最小空闲连接池大小，
     * 然后它将关闭并从池中移除
     * 以毫秒为单位的值
     **/
    private Integer soTimeout = 10000;

    /**
     * redis 连接池大小默认
     **/
    private Integer pollSize = 10;

    /**
     * redis 连接池的配置
     **/
    private RedisProperties.Pool pool = new RedisProperties.Pool();
}
