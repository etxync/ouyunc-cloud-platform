package com.ouyunc.cache.redisson.properties;

import com.ouyunc.cache.redisson.enums.RedissonEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author fangzhenxun
 * @Description: redis 核心配置
 * @Version V1.0 cache.redis
 **/
@Data
public class RedissonPrimaryProperties {

    /**
     * redis 默认单例
     **/
    RedissonEnum primary =  RedissonEnum.STANDALONE;
}
