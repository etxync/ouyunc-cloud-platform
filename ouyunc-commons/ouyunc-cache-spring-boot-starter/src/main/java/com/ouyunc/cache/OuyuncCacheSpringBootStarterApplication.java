package com.ouyunc.cache;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.cache"})
@Configuration
public class OuyuncCacheSpringBootStarterApplication {

}
