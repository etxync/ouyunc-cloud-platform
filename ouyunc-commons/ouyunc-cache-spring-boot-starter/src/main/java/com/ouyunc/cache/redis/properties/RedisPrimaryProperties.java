package com.ouyunc.cache.redis.properties;

import com.ouyunc.cache.redis.enums.RedisEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author fangzhenxun
 * @Description: redis 核心配置
 * @Version V1.0
 **/
@Data
public class RedisPrimaryProperties {

    /**
     * redis 默认单例
     **/
    RedisEnum primary =  RedisEnum.STANDALONE;
}
