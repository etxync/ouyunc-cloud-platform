package com.ouyunc.cache.redisson.properties;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;


/**
 * @author fangzhenxun
 * @date 2020/1/9 13:09
 * @description redisson 哨兵模式的配置 cache.redis.sentinel.redisson
 */
@Data
public class SentinelRedissonProperties {

    /**
     * redis 主节点的名称（在哨兵配置文件中配置的）
     **/
    private String master = "mymaster";

    /**
     * redis 主机节点地址 + 端口号以英文逗号隔开
     **/
    private String nodes = "127.0.0.1:26379";

    /**
     * redis 连接数据库配置,默认0
     **/
    private Integer database = 0;

    /**
     * redis 密码
     **/
    private String password;

    /**
     * redis 连接超时时间
     **/
    private Integer timeout = 1000;
    /**
     * redis 连接超时时间
     **/
    private Integer connTimeout = 10000;

    /**
     * 如果池连接未用于<code>超时</code>时间
     * 当前连接数大于最小空闲连接池大小，
     * 然后它将关闭并从池中移除
     * 以毫秒为单位的值
     **/
    private Integer soTimeout = 10000;

    /**
     * redis 连接池大小默认
     **/
    private Integer pollSize = 200;

    /**
     * redis lettuce 连接池的配置
     **/
    private RedisProperties.Pool pool = new RedisProperties.Pool();

    /**
     * @author fangzhenxun
     * @description  将nodes ,nodes 的格式是用英文逗号隔开的host:port，
     * @date  2020/1/9 15:15
     * @param
     * @return java.lang.String
     **/
    public String[] getNodes() {
        String[] sentinelHostAndPorts = new String[0];
        if (StringUtils.isNotBlank(nodes)) {
            sentinelHostAndPorts = Arrays.stream(nodes.split(",")).map(node -> "redis://" + node.trim()).collect(Collectors.toList()).toArray(new String[0]);
        }
        return sentinelHostAndPorts;
    }
}
