package com.ouyunc.cache.redisson.strategy.impl;

import com.ouyunc.cache.redisson.enums.RedissonEnum;
import com.ouyunc.cache.redisson.properties.ClusterRedissonProperties;
import com.ouyunc.cache.redisson.properties.SentinelRedissonProperties;
import com.ouyunc.cache.redisson.strategy.RedissonStrategy;
import com.ouyunc.common.context.SpringContextHolder;
import com.ouyunc.common.utils.YmlUtil;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.springframework.util.StringUtils;

/**
 * @author fangzhenxun
 * @date 2020/1/13 16:24
 * @description redisson 的集群配置 （待测试）
 */
public class ClusterRedissonStrategy implements RedissonStrategy {


    private static final ClusterRedissonProperties clusterRedissonProperties;

    static {
        clusterRedissonProperties = YmlUtil.getActiveProfileValue("cache.redis.cluster.redisson", ClusterRedissonProperties.class);
    }

    /**
     * @author fangzhenxun
     * @description 标识该策略类是集群
     * @date  2020/1/13 16:24
     * @param
     * @return com.xyt.cache.config.redis.redisson.enums.RedisEnum
     **/
    @Override
    public RedissonEnum getType() {
        return RedissonEnum.CLUSTER;
    }


    /**
     * @author fangzhenxun
     * @description
     * @date  2020/1/13 16:25
     * @param
     * @return org.redisson.config.Config
     **/
    @Override
    public Config buildConfig(int database) {
        Config config = new Config();
        ClusterServersConfig clusterServersConfig = config.useClusterServers()
                .addNodeAddress(clusterRedissonProperties.getNodes())
                .setScanInterval(clusterRedissonProperties.getScanInterval())
                .setIdleConnectionTimeout(clusterRedissonProperties.getSoTimeout())
                .setConnectTimeout(clusterRedissonProperties.getConnTimeout())
                .setTimeout(clusterRedissonProperties.getTimeout())
                .setRetryAttempts(clusterRedissonProperties.getRetryAttempts())
                .setRetryInterval(clusterRedissonProperties.getRetryInterval())
                .setMasterConnectionPoolSize(clusterRedissonProperties.getPollSize())
                .setSlaveConnectionPoolSize(clusterRedissonProperties.getPollSize());

        //如果密码不为空则设置密码
        if (!StringUtils.hasLength(clusterRedissonProperties.getPassword())) {
            clusterServersConfig.setPassword(clusterRedissonProperties.getPassword());
        }
        return config;
    }
}
