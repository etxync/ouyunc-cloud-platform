package com.ouyunc.cache.redisson.strategy.impl;

import com.ouyunc.cache.redis.properties.ClusterRedisProperties;
import com.ouyunc.cache.redisson.enums.RedissonEnum;
import com.ouyunc.cache.redisson.properties.StandaloneRedissonProperties;
import com.ouyunc.cache.redisson.strategy.RedissonStrategy;
import com.ouyunc.common.context.SpringContextHolder;
import com.ouyunc.common.utils.YmlUtil;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.util.StringUtils;

/**
 * @author fangzhenxun
 * @date 2020/1/10 17:09
 * @description 单例redisson具体策略实现类
 */
public class StandaloneRedissonStrategy implements RedissonStrategy {

    private static final StandaloneRedissonProperties standaloneRedissonProperties;

    static {
        standaloneRedissonProperties = YmlUtil.getActiveProfileValue("cache.redis.standalon.redisson", StandaloneRedissonProperties.class);
    }
    /**
     * @author fangzhenxun
     * @description 标识该类是某种redisson策略
     * @date  2020/1/10 17:20
     * @param
     * @return com.xyt.cache.config.redis.lettuce.enums.RedisEnum
     **/
    @Override
    public RedissonEnum getType() {
        return RedissonEnum.STANDALONE;
    }

    /**
     * @author fangzhenxun
     * @description  创建redisson的配置类
     * @date  2020/1/10 17:22
     * @param
     * @return org.redisson.config.Config
     **/
    @Override
    public Config buildConfig(int database) {
        Config config = new Config();
        SingleServerConfig singleServerConfig = config.useSingleServer()
                .setAddress("redis://" + standaloneRedissonProperties.getHost() + ":" + standaloneRedissonProperties.getPort())
                .setDatabase(standaloneRedissonProperties.getDatabase())
                .setConnectTimeout(standaloneRedissonProperties.getConnTimeout())
                .setTimeout(standaloneRedissonProperties.getTimeout())
                .setIdleConnectionTimeout(standaloneRedissonProperties.getSoTimeout())
                .setConnectionMinimumIdleSize(standaloneRedissonProperties.getPollSize())
                .setConnectionPoolSize(standaloneRedissonProperties.getPollSize());
        //如果密码不为空则设置密码
        if (StringUtils.hasLength(standaloneRedissonProperties.getPassword())) {
            singleServerConfig.setPassword(standaloneRedissonProperties.getPassword());
        }
        if (0 <= database && database <= 16) {
            singleServerConfig.setDatabase(database);
        }
        return config;
    }
}
