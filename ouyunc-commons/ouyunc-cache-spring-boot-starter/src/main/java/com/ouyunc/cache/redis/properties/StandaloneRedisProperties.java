package com.ouyunc.cache.redis.properties;

import lombok.Data;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author fangzhenxun
 * @date 2020/1/8 20:00
 * @description redis 单例模式的属性配置类
 * @ConfigurationProperties(prefix = "cache.redis.standalon") 来注入配置文件的值，或者使用@value() 方式来注入
 * @ConfigurationProperties 的 POJO类的命名比较严格,因为它必须和prefix的后缀名要一致, 不然值会绑定不上, 特殊的后缀名是“driver-class-name”这种带横杠的情况,在POJO里面的命名规则是 下划线转驼峰 就可以绑定成功，所以就是 “driverClassName”
 * 注意：这里使用属性RedisProperties.Lettuce 是为了接受从配置文件读取lettuce的pool连接池的配置属性，其实自己写也是可以的 cache.redis.standalon.lettuce
 */
@Data
public class StandaloneRedisProperties{

    /**
     * redis 连接数据库配置,默认0
     **/
    private Integer database = 0;

    /**
     * redis 主机地址
     **/
    private String host = "127.0.0.1";

    /**
     * redis 主机端口号
     **/
    private Integer port = 6379;

    /**
     * redis 主机密码
     **/
    private String password;

    /**
     * redis 连接超时时间
     **/
    private Long timeout = 1000L;

    /**
     * redis lettuce 连接池的配置
     **/
    private RedisProperties.Pool pool = new RedisProperties.Pool();
}
