package com.ouyunc.apidoc.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.ouyunc.apidoc.properties.ApiDocProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * api 文档配置, 配置详情：https://blog.csdn.net/qq_26103133/article/details/117969518
 */
@Configuration
//@EnableSwagger2  // 开启swagger2
//@EnableOpenApi   // 开启swagger3 可不写
@EnableKnife4j   // 开启knife4j  可不写
public class ApiDocConfig {

    @Autowired
    private ApiDocProperty apiDocProperty;


    @Bean
    public Docket apiDoc(){
        Docket docket = new Docket(DocumentationType.OAS_30) // 使用oas_30 规范
                .enable(apiDocProperty.isEnable());
                if (apiDocProperty.isEnableGroup()) {
                    docket.groupName(apiDocProperty.getGroupName());
                }
                return docket.apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title(apiDocProperty.getTitle())
                .description(apiDocProperty.getDescription())
                .version(apiDocProperty.getVersion())
                //apiDocProperty.getContact()
                .contact(new Contact(apiDocProperty.getContact().getName(),apiDocProperty.getContact().getUrl(),apiDocProperty.getContact().getEmail()))
                .build();
    }

}
