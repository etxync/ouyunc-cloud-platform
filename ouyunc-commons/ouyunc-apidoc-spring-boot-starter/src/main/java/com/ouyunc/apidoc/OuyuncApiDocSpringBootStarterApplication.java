package com.ouyunc.apidoc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * knife4j 访问地址：http://localhost:8080/doc.html
 * Swagger2.0访问地址：http://localhost:8080/swagger-ui.html
 * Swagger3.0访问地址：http://localhost:8080/swagger-ui/index.html
 */
@ComponentScan(basePackages = {"com.ouyunc.apidoc"})
@Configuration
public class OuyuncApiDocSpringBootStarterApplication {


}
