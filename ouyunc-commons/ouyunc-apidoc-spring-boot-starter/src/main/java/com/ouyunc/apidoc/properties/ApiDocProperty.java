package com.ouyunc.apidoc.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "api-doc")
public class ApiDocProperty {

    /**
     * 是否开启api 文档
     **/
    private boolean enable = true;

    /**
     * 是否开启分组,默认不开启
     **/
    private boolean enableGroup = false;

    /**
     *  只有开启分组后，设置分组名称才会生效
     **/
    private String groupName = "default";


    /**
     *  文档 标题title
     **/
    private String title = "";

    /**
     *  文档 描述
     **/
    private String description = "";

    /**
     *  文档 版本
     **/
    private String version = "";

    /**
     *  文档 联系信息
     **/
    private Contact contact = new Contact();

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isEnableGroup() {
        return enableGroup;
    }

    public void setEnableGroup(boolean enableGroup) {
        this.enableGroup = enableGroup;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    // 联系信息
    public class Contact {
        private  String name;
        private  String url;
        private  String email;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
