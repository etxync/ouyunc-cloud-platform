package com.ouyunc.transaction;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.transaction"})
@Configuration
public class OuyuncTransactionSpringBootStarterApplication {


}
