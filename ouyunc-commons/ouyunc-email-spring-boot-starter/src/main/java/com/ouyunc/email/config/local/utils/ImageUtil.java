package com.ouyunc.email.config.local.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.InputStream;

/**
 * @Author fangzhenxun
 * @Description 图片工具类
 * @Date 2020/3/23 13:56
 **/
public class ImageUtil {



    /**
     * @Author fangzhenxun
     * @Description  传入文件   通过读取文件并获取其width及height的方式，来判断判断当前文件是否图片,(当然也可以使用后缀名或文件魔数来进行判断)
     * @Date 2020/3/23 13:57
     * @param imageFile
     * @return boolean
     **/
    public static boolean isImage(File imageFile) {
        if (imageFile == null || !imageFile.exists()) {
            return false;
        }
        Image img = null;
        try {
            img = ImageIO.read(imageFile);
            if (img == null || img.getWidth(null) <= 0 || img.getHeight(null) <= 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            img = null;
        }
    }


    /**
     * @Author fangzhenxun
     * @Description  传入文件输入流   通过读取文件并获取其width及height的方式，来判断判断当前文件是否图片,(当然也可以使用后缀名或文件魔数来进行判断)
     * @Date 2020/3/23 14:06
     * @param fileInputStream
     * @return boolean
     **/
    public static boolean isImage(InputStream fileInputStream) {
        if (fileInputStream == null) {
            return false;
        }
        Image img = null;
        try {
            img = ImageIO.read(fileInputStream);
            if (img == null || img.getWidth(null) <= 0 || img.getHeight(null) <= 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            img = null;
        }
    }

}
