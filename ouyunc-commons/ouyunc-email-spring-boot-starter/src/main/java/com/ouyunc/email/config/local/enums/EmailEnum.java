package com.ouyunc.email.config.local.enums;

/**
 * @Author fangzhenxun
 * @Description 邮件相关配置参数枚举
 * @Date 2020/3/17 13:37
 **/
public enum EmailEnum {


    /**
     * 邮箱域名地址
     */
    HOST("mail.host"),

    /**
     * 邮箱协议
     */
    PROTOCOL("mail.transport.protocol"),


    /**
     * 是否开启邮箱授权认证
     */
    AUTH("mail.smtp.auth");



    private String value;


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    EmailEnum(String value) {
        this.value = value;
    }
}
