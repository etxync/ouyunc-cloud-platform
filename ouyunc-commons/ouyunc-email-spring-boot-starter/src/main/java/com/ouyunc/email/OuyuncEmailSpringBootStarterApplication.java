package com.ouyunc.email;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.email"})
@Configuration
public class OuyuncEmailSpringBootStarterApplication {}
