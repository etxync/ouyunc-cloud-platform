package com.ouyunc.email.config;


import com.ouyunc.email.config.local.bo.EmailParam;

/**
 * @Author fangzhenxun
 * @Description 邮件操作接口，定义几种类型的发送场景
 * @Date 2020/3/16 16:18
 **/
public interface EmailTemplate {


    /**
     * @Author fangzhenxun
     * @Description  本地发送邮件
     * @Date 2020/3/16 16:34
     * @param emailParam
     * @return void
     **/
    void sendMail(EmailParam emailParam);


}
