package com.ouyunc.email.config.local.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author fangzhenxun
 * @Description 邮件发送服务其的必要参数配置
 * @Date 2020/3/17 12:24
 **/
@Data
@Component
@ConfigurationProperties(prefix = "mail")
public class EmailProperties {

    /**
     * 邮件的服务器域名(有多种邮件域名地址：如腾讯：smtp.qq.com， 网易：smtp.163.com 等)，唯一
     **/
    private String emailHost = "smtp.qq.com";

    /**
     * 邮箱协议
     */
    private String emailProtocol = "smtp" ;

    /**
     * 发件人地址
     */
    private String fromAddress = "664936598@qq.com" ;


    /**
     * 发件人用户授权码 注意：不是用户名密码 可以自行查看相关邮件服务器怎么查看
     */
    private String authorizationCode = "123456";


    /**
     * 设置回复应指向的地址。
     * （通常只指定一个地址。）
     */
    private String replayAddress = "664936598@qq.com";

    /**
     * 是否开启邮箱授权（默认需要请求认证）
     */
    private String emailAuth = "true" ;

    /**
     * 邮件发件人昵称
     **/
    private String emailNick = "ouyunc";



}
