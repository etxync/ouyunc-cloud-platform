package com.ouyunc.email.config.local.bo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author fangzhenxun
 * @Description 邮件的相关参数
 * @Date 2020/3/16 16:53
 **/
@Data
@ToString
public class EmailParam implements Serializable {


    /**
     * 主要收件人地址列表 多用户使用英文逗号分隔 必传
     */
    private String toAddress;

    /**
     * “抄送”（复写）收件人,多用户使用英文逗号分隔 选传
     */
    private String ccAddress;

    /**
     * “密件抄送”收件人,多用户使用英文逗号分隔 选传
     */
    private String bccAddress;


    /**
     * 设置“主题”标题字段 选传
     */
    private String subject;

    /**
     * 设置此消息内容的方便方法 选传
     */
    private String content;

    /**
     * 内容中嵌套的图片,key = fileName 为图片名称，value 为图片文件 选传
     */
    private Map<String, byte[]> imageFileMap;

    /**
     * 附件 key = fileName 为附件名称,  key = fileValue 为附件的文件，选传
     */
    private Map<String, byte[]> attachmentFileMap;


    /**
     * 邮件序列号（全局唯一，可以用来追踪邮件，等其他操作） 必传
     */
    private String emailSerialNumber;
}
