package com.ouyunc.web.exception;

/**
 * @Author fangzhenxun
 * @Description: 自定义异常
 * @Version V1.0
 **/
public class CustomerException extends RuntimeException{
    public CustomerException(String message) {
        super(message);
    }
}
