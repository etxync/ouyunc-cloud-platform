package com.ouyunc.web.common;


/**
 * @Author fangzhenxun
 * @Description: 当前登录用户
 * @Version V1.0
 **/
@FunctionalInterface
public interface LoginUser {


    /**
     * 当前登录人
     */
    Object principle();

}
