package com.ouyunc.web;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.web"})
@Configuration
public class OuyuncWebSpringBootStarterApplication {


}
