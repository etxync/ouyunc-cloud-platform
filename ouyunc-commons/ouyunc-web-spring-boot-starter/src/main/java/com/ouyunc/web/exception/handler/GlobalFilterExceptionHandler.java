package com.ouyunc.web.exception.handler;

import com.ouyunc.common.base.ResponseResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 自定义过滤器filter 中的异常捕获
 */
@ConditionalOnMissingBean(value = ErrorController.class)
@RestController
public class GlobalFilterExceptionHandler extends BasicErrorController {
    Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    public GlobalFilterExceptionHandler() {
        this(new DefaultErrorAttributes(), new ErrorProperties());
    }

    public GlobalFilterExceptionHandler(ErrorAttributes errorAttributes, ErrorProperties errorProperties) {
        super(errorAttributes, errorProperties);
    }

    @Override
    @RequestMapping
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        log.error("过滤器异常了");
        Map<String, Object> errorAttributes = getErrorAttributes(request, ErrorAttributeOptions.of(ErrorAttributeOptions.Include.STACK_TRACE));
        HttpStatus status = getStatus(request);
        String message = errorAttributes.get("message").toString();
        if (StringUtils.isBlank(message)) {
            message = errorAttributes.get("error").toString();
        }
        return new ResponseEntity(ResponseResult.common(status.value(), message, null,false), status);
    }
}
