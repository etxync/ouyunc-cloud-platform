package com.ouyunc.web.annotation;


import com.ouyunc.common.constant.enums.CurrentLoginUserSourceEnum;

import java.lang.annotation.*;

/**
 * @Author fangzhenxun
 * @Description 拦截请求方法参数中带有该注解的方法，通过某种策略（解析请求头/读redis）获取当前登录用户，并set到请求方法中
 */
@Target({ ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CurrentLoginUser {
    /**
     * 获取当前登录用户的来源方式, 默认值先从请求头中获取当前登录人
     */
    CurrentLoginUserSourceEnum source() default CurrentLoginUserSourceEnum.HEADER_REDIS;
}
