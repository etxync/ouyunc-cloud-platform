package com.ouyunc.sms.config.alibaba.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author fangzhenxun
 * @Description 阿里短信属性配置
 * @Date 2020/3/18 19:22
 **/
@Data
@Component
@ConfigurationProperties(prefix = "sms.alibaba")
public class AliSmsProperties {

    /**
     * 访问密钥 ID。AccessKey 用于调用 API
     **/
    private String accessKey;

    /**
     * 密钥值 用于调用 API
     **/
    private String accessSecret;

    /**
     * 发送邮件的服务域名地址
     **/
    private String serverAddress;

    /**
     * API 的版本号，格式为 YYYY-MM-DD。取值范围：2017-05-25
     **/
    private String apiVersion;

    /**
     * API支持的RegionID，如短信API的值为：cn-hangzhou
     **/
    private String regionId;


}
