package com.ouyunc.sms;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.sms"})
@Configuration
public class OuyuncSmsSpringBootStarterApplication {

}
