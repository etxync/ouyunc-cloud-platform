package com.ouyunc.sms.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.ResponseEntity;

/**
 * @Author fangzhenxun
 * @Description 短信操作类
 * @Date 2020/3/18 15:13
 **/
public interface SmsTemplate {

    /**
     * @Author fangzhenxun
     * @Description  单个手机号短信发送接口方法
     * 批零发送短信，不好控制发送结果，这里暂时不做封装
     * @Date 2020/3/18 15:51
     * @param phoneNum          手机号
     * @param signName          短信签名
     * @param templateCode      短信模板code（在控制台中设置）
     * @param templateParam     模板中的参数（使用map来进行传参）
     * @param smsSerialNumber   消息全局唯一序列号（用来记录该消息）
     * @return void
     **/
    ResponseEntity sendSms(String phoneNum, String signName, String templateCode, JSON templateParam, String smsSerialNumber);


}
