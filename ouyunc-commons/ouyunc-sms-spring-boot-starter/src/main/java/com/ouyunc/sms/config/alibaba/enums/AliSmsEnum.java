package com.ouyunc.sms.config.alibaba.enums;

/**
 * @Author fangzhenxun
 * @Description 阿里短信 所涉及的key枚举
 * @Date 2020/3/18 19:49
 **/
public enum AliSmsEnum {

    /**
     * 单个手机号发送api 名称
     */
    API_ACTION_SINGLE("SendSms"),


    /**
     * 单条发送签名
     */
    SIGN_NAME_SINGLE("SignName"),


    /**
     * 单个手机号
     */
    PHONE_NUMBERS_SINGLE("PhoneNumbers"),


    /**
     * 模版code
     */
    TEMPLATE_CODE("TemplateCode"),

    /**
     * 模版参数替换
     */
    TEMPLATE_PARAM("TemplateParam"),

    /**
     * 外部流水扩展字段,用于扩展该消息和业务的关系
     */
    OUT_ID("OutId");





    private String value;


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    AliSmsEnum(String value) {
        this.value = value;
    }
}
