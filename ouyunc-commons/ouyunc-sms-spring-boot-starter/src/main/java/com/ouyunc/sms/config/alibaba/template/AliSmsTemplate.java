package com.ouyunc.sms.config.alibaba.template;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.google.gson.reflect.TypeToken;
import com.ouyunc.sms.config.SmsTemplate;
import com.ouyunc.sms.config.alibaba.enums.AliSmsEnum;
import com.ouyunc.sms.config.alibaba.properties.AliSmsProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author fangzhenxun
 * @Description 阿里巴巴短信插件配置,注意老版本和新版本的写法
 * @Date 2020/3/18 15:30
 **/
@Component
public class AliSmsTemplate implements SmsTemplate {
    private static Logger log = LoggerFactory.getLogger(AliSmsTemplate.class);


    /**
     * 阿里属性配置文件
     **/
    @Autowired
    private AliSmsProperties aliSmsProperties;




    /**
     * @Author fangzhenxun
     * @Description  短信发送接口方法
     * @Date 2020/3/18 15:51
     * @param phoneNum          手机号
     * @param signName          短信签名
     * @param templateCode      短信模板code（在控制台中设置）
     * @param templateParam     模板中的参数（使用map来进行传参）
     * @param smsSerialNumber   消息全局唯一序列号（用来记录该消息）
     * @return void
     **/
    @Override
    public ResponseEntity sendSms(String phoneNum, String signName, String templateCode, JSON templateParam, String smsSerialNumber) {
        StringBuilder errorMsg = new StringBuilder("调用第三方短信失败,失败原因：");
        try {
            //创建配置文件类
            IClientProfile profile = DefaultProfile.getProfile(aliSmsProperties.getRegionId(), aliSmsProperties.getAccessKey(), aliSmsProperties.getAccessSecret());
            //创建发送短信客户端
            IAcsClient smsClient = new DefaultAcsClient(profile);
            //创建请求工具
            CommonRequest request = new CommonRequest();
            //设置发送短信的域名
            request.setDomain(aliSmsProperties.getServerAddress());
            //设置以什么方式请求
            request.setMethod(MethodType.POST);
            //设置请求方法
            request.setAction(AliSmsEnum.API_ACTION_SINGLE.getValue());
            //设置ap 的版本号，一般是固定的
            request.setVersion(aliSmsProperties.getApiVersion());
            //设置签名（在控制台配置好的）
            request.putQueryParameter(AliSmsEnum.SIGN_NAME_SINGLE.getValue(), signName);
            //设置需要发送的手机号（单个）
            request.putQueryParameter(AliSmsEnum.PHONE_NUMBERS_SINGLE.getValue(), phoneNum);
            //设置需要使用的短信模板code
            request.putQueryParameter(AliSmsEnum.TEMPLATE_CODE.getValue(), templateCode);
            //设置短信唯一序列码,可以通过返回的requestId再次调用api得到outId外部流水号
            if (StringUtils.isNotBlank(smsSerialNumber)) {
                request.putQueryParameter(AliSmsEnum.OUT_ID.getValue(), smsSerialNumber);
            }

            //设置模板内容参数
            request.putQueryParameter(AliSmsEnum.TEMPLATE_PARAM.getValue(), templateParam.toJSONString());
            try {
                //发送请求
                CommonResponse response = smsClient.getCommonResponse(request);
                //获取返回数据体
                Map<String,String> responseData = JSONObject.parseObject(response.getData(), new TypeToken<Map<String,String>>() {}.getType());
                log.info("返回消息体为：" + responseData);
                if (responseData.get("Code").equals("OK")) {
                    return new ResponseEntity(responseData, HttpStatus.OK);
                }
                errorMsg.append(responseData.get("Message"));
            } catch (ClientException e) {
                e.printStackTrace();
                log.error("调用第三方短信失败,失败原因：{}", e.getMessage());
            }
        }catch (Exception e) {
            e.printStackTrace();
            log.error("调用第三方短信失败,失败原因：{}", e.getMessage());
        }
        return new ResponseEntity(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
