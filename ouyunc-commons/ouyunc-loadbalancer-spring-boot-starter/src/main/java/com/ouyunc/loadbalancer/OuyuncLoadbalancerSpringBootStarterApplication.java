package com.ouyunc.loadbalancer;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.loadbalancer"})
@Configuration
public class OuyuncLoadbalancerSpringBootStarterApplication {


}
