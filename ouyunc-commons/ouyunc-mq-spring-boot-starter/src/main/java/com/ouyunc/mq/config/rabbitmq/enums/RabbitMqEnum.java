package com.ouyunc.mq.config.rabbitmq.enums;

/**
 * @Author fangzhenxun
 * @Description: mq 消息枚举类
 * @Date 2020/2/29 15:49
 * @Version V1.0
 **/
public enum RabbitMqEnum {

    /**
     * 单例
     */
    STANDALONE("单例"),


    /**
     * 集群，（如果针对rabbitmq 则是镜像集群）
     */
    CLUSTER("集群");

    /**
     * mq的模式类型
     */
    private String mqModel;


    RabbitMqEnum(String mqModel) {
        this.mqModel = mqModel;
    }

    public String getMqModel() {
        return mqModel;
    }

    public void setMqModel(String mqModel) {
        this.mqModel = mqModel;
    }
}
