package com.ouyunc.mq.config.rabbitmq.builder;

/**
 * @Author fangzhenxun
 * @Description: rabbitmq 抽象建造者
 * @Date 2020/2/28 21:00
 * @Version V1.0
 **/
public interface RabbitMqBuilder<T> {


    /**
     * @Author fangzhenxun
     * @Description 抽象建造方法
     * @date 2020/2/28 21:01
     * @param
     * @return T
     */
    T build();
}
