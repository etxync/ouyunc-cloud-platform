package com.ouyunc.mq.config.kafka.properties;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Author fangzhenxun
 * @Description 定义公共的属行
 * @Date 2020/3/16 12:30
 **/
@Data
public class CommonProperties {
    /**
     * common的连接
     **/
    private List<String> bootstrapServers = new ArrayList(Collections.singletonList("localhost:9092"));

}
