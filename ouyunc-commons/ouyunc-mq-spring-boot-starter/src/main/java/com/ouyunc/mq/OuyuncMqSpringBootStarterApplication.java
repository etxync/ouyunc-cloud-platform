package com.ouyunc.mq;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.ouyunc.mq"})
@Configuration
public class OuyuncMqSpringBootStarterApplication {
}
