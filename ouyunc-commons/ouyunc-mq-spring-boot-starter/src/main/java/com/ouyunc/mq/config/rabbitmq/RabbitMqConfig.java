package com.ouyunc.mq.config.rabbitmq;

import com.ouyunc.mq.config.rabbitmq.builder.impl.RabbitTemplateBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author fangzhenxun
 * @Description: rabbitmq 消息相关配置（使用建造者模式来进行构建）
 * @Date 2020/2/28 20:55
 * @Version V1.0
 **/
@Configuration
public class RabbitMqConfig {

    /**
     * rabbitmq模板建造者
     */
    @Autowired
    private RabbitTemplateBuilder rabbitTemplateBuilder;


    /**
     * @Author fangzhenxun
     * @Description 构建rabbit操作模板
     * @date 2020/2/28 20:58
     * @param
     * @return org.springframework.amqp.rabbit.core.RabbitTemplate
     */
    @Bean
    public RabbitTemplate RabbitTemplate() {
        return rabbitTemplateBuilder.build();
    }

}
