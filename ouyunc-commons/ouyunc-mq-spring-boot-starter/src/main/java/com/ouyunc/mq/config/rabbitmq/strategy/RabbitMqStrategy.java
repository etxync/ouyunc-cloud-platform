package com.ouyunc.mq.config.rabbitmq.strategy;

import com.ouyunc.mq.config.rabbitmq.enums.RabbitMqEnum;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;

/**
 * @Author fangzhenxun
 * @Description: rabbitmq 策略
 * @Date 2020/2/29 17:36
 * @Version V1.0
 **/
public interface RabbitMqStrategy {


    /**
     * 标识rabbitmq实现类的模式类型
     */
    RabbitMqEnum getType();

    /**
     * @Author fangzhenxun
     * @Description 获取rabbitmq连接工厂
     * @date 2020/2/29 17:38
     * @param
     * @return org.springframework.amqp.rabbit.connection.CachingConnectionFactory
     */
    CachingConnectionFactory buildConnectionFactory();
}
