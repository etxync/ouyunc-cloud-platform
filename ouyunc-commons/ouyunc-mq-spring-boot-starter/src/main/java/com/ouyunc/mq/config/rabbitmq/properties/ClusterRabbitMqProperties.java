package com.ouyunc.mq.config.rabbitmq.properties;

import lombok.Data;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author fangzhenxun
 * @description redis 镜像集群模式的属性配置类
 * @ConfigurationProperties(prefix = "mq.rabbit.standalon") 来注入配置文件的值，或者使用@value() 方式来注入
 * @ConfigurationProperties 的 POJO类的命名比较严格,因为它必须和prefix的后缀名要一致, 不然值会绑定不上, 特殊的后缀名是“driver-class-name”这种带横杠的情况,在POJO里面的命名规则是 下划线转驼峰 就可以绑定成功，所以就是 “driverClassName”
 * @Date 2020/2/29 17:59
 * @Version V1.0
 **/
@Data
@Component
@ConfigurationProperties(prefix = "mq.rabbit.cluster")
public class ClusterRabbitMqProperties {

    /**
     * rabbitmq 连接地址,默认本机
     **/
    private String addresses = "127.0.0.1:5672";

    /**
     * rabbitmq 连登陆用户名
     **/
    private String username = "admin";

    /**
     * rabbitmq 登陆密码
     **/
    private String password = "admin";

    /**
     * 连接到rabbitMQ的vhost
     **/
    private String virtualHost = "/";

    /**
     * 默认开启confirm模式
     **/
    private CachingConnectionFactory.ConfirmType confirmType = CachingConnectionFactory.ConfirmType.CORRELATED;

    /**
     * 默认开启return机制
     **/
    private boolean publisherReturns = true;


    /**
     * rabbit 缓存
     **/
    private final RabbitProperties.Cache cache = new RabbitProperties.Cache();

    /**
     * rabbit 监听
     **/
    private final RabbitProperties.Listener listener = new RabbitProperties.Listener();

    /**
     * rabbit 模板
     **/
    private final RabbitProperties.Template template = new RabbitProperties.Template();


    /**
     * @Author fangzhenxun
     * @Description 去除字符串中的空字符
     * @date 2020/3/2 14:01
     * @param
     * @return java.lang.String
     */
    public String getAddresses() {
        return addresses.trim().replaceAll("\\s+", "");
    }
}
