package com.ouyunc.mq.config.kafka.strategy;

import com.ouyunc.mq.config.kafka.enums.KafkaMqEnum;
import org.springframework.kafka.core.ProducerFactory;

/**
 * @Author fangzhenxun
 * @Description kafka 策略抽象类
 * @Date 2020/3/13 11:10
 **/
public interface KafkaMqStrategy<K, V> {


    /**
     * 标识rabbitmq实现类的模式类型
     */
    KafkaMqEnum getType();

    /**
     * @Author fangzhenxun
     * @Description   生产者工厂
     * @Date 2020/3/13 11:20
     * @param
     * @return org.springframework.kafka.core.ProducerFactory<K,V>
     **/
    ProducerFactory<K, V> buildProducerFactory();
}
